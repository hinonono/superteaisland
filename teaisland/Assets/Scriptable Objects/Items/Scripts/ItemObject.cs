using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType
{
    Food,
    Equipment,
    Tea,
    Topping,
    Tiaocha,
    Default
}

public enum Attributes
{
    Agility,
    Intellect,
    Stamina,
    Strength
}

public abstract class ItemObject : ScriptableObject
{
    public int Id;
    public Sprite uiDisplay;
    public ItemType type;
    [TextArea(5, 10)]
    public string itemName;

    [TextArea(2, 10)]
    public string flavor;
    [TextArea(2, 10)]
    public string effect;
    [TextArea(2, 10)]
    public string coordination;



    public ItemBuff[] buffs;

    public Item CreateItem()
    {
        Item newItem = new Item(this);
        return newItem;
    }
}

[System.Serializable]
public class Item
{
    public string Name;

    //繼承自上面class的item name，查詢食譜時要用
    public string itemName;


    public int Id;
    public ItemBuff[] buffs;

    public string flavor;
    public string effect;
    public string coordination;

    public string fromTea;
    public string fromTopping;
    public string customName = "尚未設定";

    public ItemType type;

    public Guid guid;

    public Item(ItemObject item)
    {
        Name = item.name;
        Id = item.Id;

        itemName = item.itemName;

        flavor = item.flavor;
        effect = item.effect;
        coordination = item.coordination;

        type = item.type;

        guid = Guid.NewGuid();

        buffs = new ItemBuff[item.buffs.Length];
        for (int i = 0; i < buffs.Length; i++)
        {
            buffs[i] = new ItemBuff(item.buffs[i].min, item.buffs[i].max)
            {
                attribute = item.buffs[i].attribute
            };
        }
    }

    public Item(TiaochaObject item)
    {
        Name = item.name;
        Id = item.Id;

        itemName = item.itemName;

        flavor = item.flavor;
        effect = item.effect;
        coordination = item.coordination;

        type = item.type;

        guid = Guid.NewGuid();

        buffs = new ItemBuff[item.buffs.Length];
        for (int i = 0; i < buffs.Length; i++)
        {
            buffs[i] = new ItemBuff(item.buffs[i].min, item.buffs[i].max)
            {
                attribute = item.buffs[i].attribute
            };
        }

        fromTea = item.fromTea;
        fromTopping = item.fromTopping;
    }

    public void SetCustomName(string name)
    {
        customName = name;
    }
}

[System.Serializable]
public class ItemBuff
{
    public Attributes attribute;
    public int value;
    public int min;
    public int max;
    public ItemBuff(int _min, int _max)
    {
        min = _min;
        max = _max;
        GenerateValue();
    }

    public void GenerateValue()
    {
        value = UnityEngine.Random.Range(min, max);
    }
}
