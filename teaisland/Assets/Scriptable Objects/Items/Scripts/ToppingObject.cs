using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Topping Object", menuName = "Inventory System/Items/Topping")]
public class ToppingObject : ItemObject
{
    private void Awake()
    {
        type = ItemType.Topping;
    }
}
