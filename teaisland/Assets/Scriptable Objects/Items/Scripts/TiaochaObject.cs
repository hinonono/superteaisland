using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tiaocha Object", menuName = "Inventory System/Items/Tiaocha")]
public class TiaochaObject : ItemObject
{
    public string fromTea;
    public string fromTopping;

    private void Awake()
    {
        type = ItemType.Tiaocha;
    }
}
