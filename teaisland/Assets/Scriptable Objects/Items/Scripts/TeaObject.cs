using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tea Object", menuName = "Inventory System/Items/Tea")]
public class TeaObject : ItemObject
{
    private void Awake()
    {
        type = ItemType.Tea;
    }
}
