using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundItem : MonoBehaviour
{
    public ItemObject item;

    private void Start()
    {
        if (item == null)
        {
            Debug.LogError("Groud Item on " + gameObject.name + " hasn't been set yet.");
        }
    }
}
