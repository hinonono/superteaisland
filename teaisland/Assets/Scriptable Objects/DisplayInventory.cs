using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class DisplayInventory : MonoBehaviour
{
    public GameObject inventoryPrefab;
    public InventoryObject inventory;
    private PlayerInventory playerInventory;

    public int X_START;
    public int Y_START;
    public int X_SPACE_BETWEEN_ITEM;
    public int NUMBER_OF_COLUMN;
    public int Y_SPACE_BETWEEN_ITEMS;
    Dictionary<InventorySlot, GameObject> itemDisplayed = new Dictionary<InventorySlot, GameObject>();

    [SerializeField] private PlaySound playSound;

    private List<GameObject> child; //這個物件底下的所有inventory prefab

    public enum UpdateMode
    {
        Default,
        Forced //強制性清空並重新顯示display
    }

    private void OnEnable()
    {
        playSound = gameObject.GetComponent<PlaySound>();
    }

    private void Awake()
    {
        UpdateAllChild();
    }

    // Start is called before the first frame update
    void Start()
    {
        playerInventory = GameObject.Find("Player").GetComponent<PlayerInventory>();
        playerInventory.onItemPicked += PlayerInventory_onItemPicked;
        playerInventory.onItemDeleted += PlayerInventory_onItemDeleted;
        playerInventory.OnInventoryChanged += PlayerInventory_OnInventoryChanged;

        UpdateDisplay();
    }

    public void UpdateAllChild()
    {
        child = new List<GameObject>();

        Array array = GameObject.FindGameObjectsWithTag("InventoryChild");

        foreach (var item in array)
        {
            child.Add((GameObject)item);
        }
    }

    public void DestroyAllChild()
    {
        foreach (var item in child)
        {
            Destroy(item);
        }
    }

    private void PlayerInventory_OnInventoryChanged()
    {
        UpdateDisplay();
    }

    private void PlayerInventory_onItemDeleted()
    {
        UpdateDisplay();
    }

    private void PlayerInventory_onItemPicked(string itemName, int quantity)
    {
        UpdateDisplay();
    }

    public void UpdateDisplay(UpdateMode mode = UpdateMode.Default)
    {
        if (mode == UpdateMode.Forced)
        {
            UpdateAllChild();
            DestroyAllChild();
            itemDisplayed.Clear();
            UpdateDisplay();
        }

        if (inventory.Container.Items.Count == 0)
        {
            itemDisplayed.Clear();
        }

        for (int i = 0; i < inventory.Container.Items.Count; i++)
        {
            InventorySlot slot = inventory.Container.Items[i];

            if (itemDisplayed.ContainsKey(slot))
            {
                itemDisplayed[slot].GetComponentInChildren<TextMeshProUGUI>().text = inventory.Container.Items[i].amount.ToString("n0");
                LeanTween.scale(itemDisplayed[slot], Vector3.one, .3f).setEase(LeanTweenType.easeInCubic);
                playSound.PlayClip();
            }
            else
            {
                playSound.PlayClip();

                var obj = Instantiate(inventoryPrefab, Vector3.zero, Quaternion.identity, transform);
                obj.transform.GetChild(0).GetComponentInChildren<Image>().sprite = inventory.database.GetItem[slot.item.Id].uiDisplay;

                obj.transform.Find("Text_id").GetComponent<TextMeshProUGUI>().text = slot.item.Id.ToString();
                //obj.GetComponent<RectTransform>().localPosition = GetPosition(i);
                obj.GetComponentInChildren<TextMeshProUGUI>().text = slot.amount.ToString("n0");

                LeanTween.scale(obj, Vector3.one, .3f).setEase(LeanTweenType.easeInCubic);

                itemDisplayed.Add(inventory.Container.Items[i], obj);
            }
        }
    }

    public void CreateDisplay()
    {
        for (int i = 0; i < inventory.Container.Items.Count; i++)
        {
            InventorySlot slot = inventory.Container.Items[i];

            var obj = Instantiate(inventoryPrefab, Vector3.zero, Quaternion.identity, transform);
            obj.transform.GetChild(0).GetComponentInChildren<Image>().sprite = inventory.database.GetItem[slot.item.Id].uiDisplay;
            //obj.GetComponent<RectTransform>().localPosition = GetPosition(i);
            obj.GetComponentInChildren<TextMeshProUGUI>().text = slot.amount.ToString("n0");

            itemDisplayed.Add(slot, obj);

            playSound.PlayClip();
        }
    }

    public Vector3 GetPosition(int i)
    {
        return new Vector3(X_START + (X_SPACE_BETWEEN_ITEM * (i % NUMBER_OF_COLUMN)), Y_START + (-Y_SPACE_BETWEEN_ITEMS * (i / NUMBER_OF_COLUMN)), 0f);
    }

    public void OnDestroy()
    {
        playerInventory.onItemPicked -= PlayerInventory_onItemPicked;
        playerInventory.onItemDeleted -= PlayerInventory_onItemDeleted;
    }


}