using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory System/Recipes/Recipe")]
public class Recipe : ScriptableObject
{
    //SCRIPT用途：定義Recipe Scriptable Object

    public int Id;
    public TeaObject tea;
    public ToppingObject topping;
    public TiaochaObject result;

    public string recipeName;
    public recipeType type;
    

    public enum recipeType
    {
        TeaRed,
        TeaGreen,
        TeaBlack,
        TeaCyan,
        TeaWhite,
        TeaYellow
    }

    public string getRecipeName()
    {
        return recipeName;
    }

    public TiaochaObject getResult()
    {
        return result;
    }
}
