using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tea Black Recipe", menuName = "Inventory System/Recipes/Tea Black Recipe")]

public class TeaBlackRecipe : Recipe
{
    private void Awake()
    {
        type = recipeType.TeaBlack;
    }
}
