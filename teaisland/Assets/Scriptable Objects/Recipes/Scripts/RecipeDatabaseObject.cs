using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Recipe Database", menuName = "Inventory System/Recipes/Database")]
public class RecipeDatabaseObject : ScriptableObject, ISerializationCallbackReceiver
{
    public Recipe[] recipes;
    public Dictionary<int, Recipe> GetRecipeItem = new Dictionary<int, Recipe>();

    public void OnAfterDeserialize()
    {
        for (int i = 0; i < recipes.Length; i++)
        {

            //裡頭的東西一直報錯，看加個判斷式會不會好一點
            if (recipes[i] != null)
            {
                recipes[i].Id = i;
                GetRecipeItem.Add(i, recipes[i]);
            }
        }
    }

    public void OnBeforeSerialize()
    {
        GetRecipeItem = new Dictionary<int, Recipe>();
    }
}
