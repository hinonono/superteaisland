using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tea Green Recipe", menuName = "Inventory System/Recipes/Tea Green Recipe")]

public class TeaGreenRecipe : Recipe
{
    private void Awake()
    {
        type = recipeType.TeaGreen;
    }
}
