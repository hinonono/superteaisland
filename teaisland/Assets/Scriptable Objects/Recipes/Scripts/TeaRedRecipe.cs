using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tea Red Recipe", menuName = "Inventory System/Recipes/Tea Red Recipe")]
public class TeaRedRecipe : Recipe
{
    private void Awake()
    {
        type = recipeType.TeaRed;
    }
}
