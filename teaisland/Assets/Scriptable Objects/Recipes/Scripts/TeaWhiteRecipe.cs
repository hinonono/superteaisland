using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tea White Recipe", menuName = "Inventory System/Recipes/Tea White Recipe")]

public class TeaWhiteRecipe : Recipe
{
    private void Awake()
    {
        type = recipeType.TeaWhite;
    }
}
