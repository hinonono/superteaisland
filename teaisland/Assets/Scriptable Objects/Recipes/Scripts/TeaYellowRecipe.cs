using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tea Yellow Recipe", menuName = "Inventory System/Recipes/Tea Yellow Recipe")]

public class TeaYellowRecipe : Recipe
{
    private void Awake()
    {
        type = recipeType.TeaYellow;
    }
}
