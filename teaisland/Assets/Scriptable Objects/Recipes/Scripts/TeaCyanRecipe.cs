using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tea Cyan Recipe", menuName = "Inventory System/Recipes/Tea Cyan Recipe")]

public class TeaCyanRecipe : Recipe
{
    private void Awake()
    {
        type = recipeType.TeaCyan;
    }
}
