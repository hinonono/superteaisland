using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    //SCRIPT用途：最基礎的Player Inventory。
    private static PlayerInventory _instance;

    public static PlayerInventory Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject gameObj = new GameObject("Player Inventory");
                gameObj.AddComponent<PlayerInventory>();
            }

            return _instance;
        }
    }
    //Singleton 設定END



    //DELEGATE
    public delegate void onItemTouchedDelegate(ItemObject item);
    public delegate void onItemPickedDelegate(string itemName, int quantity);

    //EVENT
    public event onItemTouchedDelegate onItemTouched;
    public event onItemPickedDelegate onItemPicked;
    public event Action onItemDeleted;
    public event Action onCollectCompleted;
    public event Action onCollectNotCompleted;
    public event Action OnInventoryChanged; //當Inventory陣列數量改變時（方便Display Inventory更新UI）


    //屬性宣告
    public InventoryObject inventory;
    private GroundItem ItemMeeted; //專門儲存遇到了什麼item
    private bool hasTopping = false; //是否已經有採集配料
    private string currentToppingName;
    private int currentToppingId;


    //OnEnable, OnDisable
    private void OnEnable()
    {
    }

    private void OnDisable()
    {
    }

    //Awake, Start, Update
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }


    //CORE FUNCTION
    private void CheckIsCollectComplete()
    {
        //VOID用途：當收集超過2種物件時通知UI Manager將按鈕設為可互動
        bool isComplete = inventory.CheckCollectIsCompleted();

        if (isComplete)
        {
            onCollectCompleted?.Invoke();
        } else
        {
            onCollectNotCompleted?.Invoke();
        }
    }

    public void AddItem(ItemObject item)
    {
        inventory.AddItem(new Item(item), 1);
        if (item.type == ItemType.Topping)
        {
            hasTopping = true;
            currentToppingName = item.itemName;
            currentToppingId = item.Id;
        }

        CheckIsCollectComplete();
        OnInventoryChanged?.Invoke();
    }

    public void DeleteItemBy(int _id)
    {
        inventory.DeleteItem(_id);
        onItemDeleted?.Invoke();

        bool _hasTopping = false; //檢查是否已經沒有topping了

        if (inventory.Container.Items.Count != 0)
        {
            for (int i = 0; i < inventory.Container.Items.Count; i++)
            {
                if (inventory.Container.Items[i].item.type == ItemType.Topping)
                {
                    _hasTopping = true;
                }
            }

            if (_hasTopping != true)
            {
                hasTopping = false;
            }
        } else
        {
            hasTopping = false;
        }

        CheckIsCollectComplete();
    }

    private void OnApplicationQuit()
    {
        this.inventory.Container.Items.Clear();
    }

    public bool GetHasTopping()
    {
        return hasTopping;
    }

    public void SetHasTopping(bool status)
    {
        hasTopping = status;
    }

    public string GetCurrentToppingName()
    {
        return currentToppingName;
    }

    public int GetCurrentToppingId()
    {
        return currentToppingId;
    }

    //UNITY EDITOR FUNCTION

}
