using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GeneralPopup : MonoBehaviour
{
    //SCRIPT用途：控制一般的POPUP行為

    //控制Pop出現與消失的速度
    private float animateSpeed = 0.5f;

    [SerializeField]
    private Transform box;
    [SerializeField]
    private CanvasGroup background;
    [SerializeField]
    private TextMeshProUGUI title;
    [SerializeField]
    private TextMeshProUGUI content;

    //DELEGATE
    public delegate void PopupIntent(String intent); 

    //EVENT
    public event PopupIntent onConfirmPressed;
    public event PopupIntent onCancelPressed;
    public event Action onPopupClosed;

    public bool getCurrentState
    {
        get
        {
            return isOpen;
        }
    }

    private bool isOpen;

    //CORE FUNC
    public void openPopup(String title, String content)
    {
        this.title.text = title;
        this.content.text = content;

        background.alpha = 0;
        background.LeanAlpha(1, animateSpeed);

        box.localPosition = new Vector2(0, -Screen.height);
        box.LeanMoveLocalY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;
        isOpen = true;
    }

    public void closePopup()
    {
        background.LeanAlpha(0, animateSpeed);
        box.LeanMoveLocalY(-Screen.height, animateSpeed).setEaseInExpo().setOnComplete(onComplete);
    }

    void onComplete()
    {
        this.title.text = "";
        this.content.text = "";
        gameObject.SetActive(false);
        isOpen = false;
        onPopupClosed?.Invoke();
    }

    //FUNC for EDITOR
    public void confirmPressed(String intent)
    {
        onConfirmPressed?.Invoke(intent);
        closePopup();
    }

    public void cancelPressed(String intent)
    {
        onCancelPressed?.Invoke(intent);
        closePopup();
    }
}
