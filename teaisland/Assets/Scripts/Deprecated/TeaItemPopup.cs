using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TeaItemPopup : MonoBehaviour
{
    //控制Pop出現與消失的速度
    public float animateSpeed = 0.5f;

    //自身相關元件
    public Transform box;
    public CanvasGroup background;
    public GameObject popup_title;
    public GameObject popup_description;
    public Image popup_image;

    private PlayerInventory playerInventory;

    //Event
    public event Action onConfirmPressed;
    public event Action onCancelPressed;

    private void OnEnable()
    {
        playerInventory = GameObject.Find("Player").GetComponent<PlayerInventory>();
        playerInventory.onItemTouched += PlayerInventory_onItemTouched;
        
    }

    private void OnDestroy()
    {
        playerInventory.onItemTouched -= PlayerInventory_onItemTouched;
    }

    private void PlayerInventory_onItemTouched(ItemObject item)
    {
        string name = item.itemName;
        string des = item.flavor;
        Sprite sprite = item.uiDisplay;

        popup_title.GetComponent<TextMeshProUGUI>().text = name;
        popup_description.GetComponent<TextMeshProUGUI>().text = des;
        popup_image.sprite = sprite;

        openPopup();
    }

    public void openPopup()
    {
        background.alpha = 0;
        background.LeanAlpha(1, animateSpeed);

        box.localPosition = new Vector2(0, -Screen.height);
        box.LeanMoveLocalY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;
    }

    public void closePopup()
    {
        background.LeanAlpha(0, animateSpeed);
        box.LeanMoveLocalY(-Screen.height, animateSpeed).setEaseInExpo().setOnComplete(onComplete);
    }

    void onComplete()
    {
        gameObject.SetActive(false);
    }


    //在Editor給確認、取消按鈕選用以下func
    public void confirmPressed()
    {
        onConfirmPressed?.Invoke();
        closePopup();
    }

    public void cancelPressed()
    {
        onCancelPressed?.Invoke();
        closePopup();
    }

    
}
