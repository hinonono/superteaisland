using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TeaInfoPopup : MonoBehaviour
{
    //控制Pop出現與消失的速度
    public float animateSpeed = 0.5f;

    //自身相關元件
    public Transform box;
    public CanvasGroup background;
    public TextMeshProUGUI popup_title;
    public TextMeshProUGUI text_flavor;
    public TextMeshProUGUI text_effect;

    public Image popup_image;

    private PlayerInventory playerInventory;

    //Event
    public event Action onConfirmPressed;

    private void OnEnable()
    {
        playerInventory = GameObject.Find("Player").GetComponent<PlayerInventory>();
        playerInventory.onItemTouched += PlayerInventory_onItemTouched;

    }

    private void OnDestroy()
    {
        playerInventory.onItemTouched -= PlayerInventory_onItemTouched;
    }

    private void PlayerInventory_onItemTouched(ItemObject item)
    {
        string name = item.itemName;
        string flavor = item.flavor;
        string effect = item.effect;
        Sprite sprite = item.uiDisplay;

        popup_title.text = name;
        text_flavor.text = flavor;
        text_effect.text = effect;
        popup_image.sprite = sprite;

        openPopup();
    }

    public void openPopup()
    {
        background.alpha = 0;
        background.LeanAlpha(1, animateSpeed);

        box.localPosition = new Vector2(0, -Screen.height);
        box.LeanMoveLocalY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;
    }

    public void closePopup()
    {
        background.LeanAlpha(0, animateSpeed);
        box.LeanMoveLocalY(-Screen.height, animateSpeed).setEaseInExpo().setOnComplete(onComplete);
    }

    void onComplete()
    {
        gameObject.SetActive(false);
    }

    public void confirmPressed()
    {
        //VOID用途： 確認按鈕按下後執行動作

        onConfirmPressed?.Invoke();
        closePopup();
    }
}
