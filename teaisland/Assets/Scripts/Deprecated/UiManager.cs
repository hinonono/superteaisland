using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UiManager : MonoBehaviour
{
    //SCRIPT用途：集中管理使用者介面元素

    [SerializeField]
    private Button nextButton;

    [SerializeField]
    private Button backButton;

    [SerializeField]
    private GameObject genralPopup;

    [SerializeField]
    private GameObject cartPopup;

    [SerializeField]
    private PlayerInventory playerInventory;

    [SerializeField]
    private DisplayInventory displayInventory;

    private GameObject teaAlert;

    public event Action onGenralpopupClosed;

    private void OnEnable()
    {
        playerInventory.onCollectCompleted += PlayerInventory_onCollectCompleted;
        playerInventory.onCollectNotCompleted += PlayerInventory_onCollectNotCompleted;

        genralPopup.GetComponent<GeneralPopup>().onCancelPressed += UiManager_onCancelPressed;
        genralPopup.GetComponent<GeneralPopup>().onConfirmPressed += UiManager_onConfirmPressed;
        genralPopup.GetComponent<GeneralPopup>().onPopupClosed += UiManager_onPopupClosed;

        teaAlert = GameObject.Find("Tea Alert");
    }

    private void UiManager_onPopupClosed()
    {
        onGenralpopupClosed?.Invoke();
    }

    private void UiManager_onConfirmPressed(string intent)
    {
        if (intent == "default")
        {
            genralPopup.GetComponent<GeneralPopup>().closePopup();
            return;
        }
    }

    private void UiManager_onCancelPressed(string intent)
    {
        if (intent == "default")
        {
            playerInventory.inventory.Clear();
            playerInventory.inventory.Clear("tiaocha");

            GoToScene(SceneSingleton.SceneList.Select_map.ToString());
            return;
        }
    }

    private void PlayerInventory_onCollectNotCompleted()
    {
        nextButton.interactable = false;
    }

    private void Start()
    {
        nextButton.interactable = false;
        displayInventory.UpdateDisplay();

        //if (SceneSingleton.Instance.getCurrentSceneName() == SceneSingleton.SceneList.Temple.ToString())
        //{
        //    Debug.Log("jowifjoiwjfowejo");
        //    Image progress_bar = GameObject.Find("Progress Bar").GetComponent<Image>();
        //    progress_bar.sprite = Resources.Load<Sprite>("Progress Bar/progress_bar_3");
        //}
    }

    private void PlayerInventory_onCollectCompleted()
    {
        teaAlert.GetComponent<TeaAlert>().openAlert("你已收集完調茶所需配料！快前往祭壇和長老會和吧！");
        nextButton.interactable = true;
    }

    private void OnDisable()
    {
        playerInventory.onCollectCompleted -= PlayerInventory_onCollectCompleted;
        playerInventory.onCollectNotCompleted -= PlayerInventory_onCollectNotCompleted;

        //genralPopup.GetComponent<GeneralPopup>().onCancelPressed += UiManager_onCancelPressed;
        //genralPopup.GetComponent<GeneralPopup>().onConfirmPressed += UiManager_onConfirmPressed;
    }

    public void GoToScene(string destination)
    {
        if (destination != null)
        {
            playerInventory.inventory.Save();
            SceneManager.LoadScene(destination);
        } else
        {
            Debug.LogError("你還沒有填入Next Button要前往哪裡！");
        }
        
    }

    public void ToggleGeneralPopup()
    {
        if (!genralPopup.activeSelf)
        {
            genralPopup.SetActive(true);

            GeneralPopup script = genralPopup.GetComponent<GeneralPopup>();
            string title = "確認離開？";
            string content = "回到選擇地圖後你將遺失目前的採集進度";

            script.openPopup(title, content);
        }
        else
        {
            genralPopup.SetActive(false);
        }
    }

    public void Normal_ToggleGeneralPopup(string title, string content)
    {
        //FUNC用途：給其他Script呼叫用，打開General Popup

        if (!genralPopup.activeSelf)
        {
            genralPopup.SetActive(true);
            GeneralPopup script = genralPopup.GetComponent<GeneralPopup>();

            script.openPopup(title, content);
        }
        else
        {
            genralPopup.SetActive(false);
        }
    }

    public void ToggleCartPopup()
    {
        int _quantity = playerInventory.inventory.TiaochaContainer.Items.Count;
        int _price = _quantity * 300;

        if (!cartPopup.activeSelf)
        {
            cartPopup.SetActive(true);

            CartPopup script = cartPopup.GetComponent<CartPopup>();
            
            script.quantity = _quantity;
            script.totalPrice = _price;

            script.OpenPopup();
        }
        else
        {
            cartPopup.SetActive(false);
        }
    }
}
