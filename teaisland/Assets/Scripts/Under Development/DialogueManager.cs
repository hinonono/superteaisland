using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class DialogueManager : MonoBehaviour
{
    //SCRIPT用途：開發中。控制dialogue進行的Manager。

    //Singleton 設定START
    private static DialogueManager _instance;

    public static DialogueManager Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject gameObj = new GameObject("Dialogue Manager");
                gameObj.AddComponent<DialogueManager>();
            }

            return _instance;
        }
    }

    void Awake()
    {
        _instance = this;
        DontDestroyOnLoad(this);
    }
    //Singleton 設定END

    //DELEGATE


    //EVENT
    public event Action OnDialogueEnded;


    //屬性宣告
    private GameObject dialogueItem;

    private DialogueMessage[] currentMessages;
    private Actor[] currenActors;
    private int activeMessage = 0;


    //OnEnable, OnDisable


    //Awake, Start, Update
    private void Start()
    {
        
    }


    //CORE FUNCTION
    public void EmptyFunction()
    {

    }

    public void OpenDialogue(DialogueMessage[] dialogueMessages, Actor[] actors)
    {
        dialogueItem = GUIManager.Instance.InstantiateDialogueItem();
        dialogueItem.GetComponent<DialogueItem>().OnDialoguePressed += NextMessage;

        dialogueItem.transform.parent = GameObject.Find("UI").transform;
        dialogueItem.GetComponent<DialogueItem>().OpenPopup();

        currentMessages = dialogueMessages;
        currenActors = actors;
        activeMessage = 0;

        DisplayMessage();
    }

    private void DisplayMessage()
    {
        
        DialogueMessage messageToDisplay = currentMessages[activeMessage];
        Actor actorToDisplay = currenActors[messageToDisplay.actorId];



        //dialogueItem.GetComponent<DialogueItem>().messageText.text = messageToDisplay.message;


        dialogueItem.GetComponent<DialogueItem>().actorName.text = actorToDisplay.name;
        dialogueItem.GetComponent<DialogueItem>().actorImage.sprite = actorToDisplay.sprite;

        StopAllCoroutines();
        StartCoroutine(TypeSentence(messageToDisplay.message));
    }

    public void NextMessage()
    {
        activeMessage++;
        if (activeMessage < currentMessages.Length)
        {
            DisplayMessage();
        } else
        {
            dialogueItem.GetComponent<DialogueItem>().ClosePopup();
            OnDialogueEnded?.Invoke();
        }
    }

    IEnumerator TypeSentence (string sentence)
    {
        dialogueItem.GetComponent<DialogueItem>().messageText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueItem.GetComponent<DialogueItem>().messageText.text += letter;
            yield return null;
        }
    }


    //UNITY EDITOR FUNCTION
}
