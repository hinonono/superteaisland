using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    //SCRIPT用途：


    //DELEGATE


    //EVENT
    public event Action OnDialogueEnded;


    //屬性宣告
    public DialogueMessage[] dialogueMessages;
    public Actor[] actors;

    //OnEnable, OnDisable


    //Awake, Start, Update


    //CORE FUNCTION
    public void StartDialogue()
    {
        DialogueManager.Instance.EmptyFunction();
        DialogueManager.Instance.OpenDialogue(dialogueMessages, actors);
    }

}

[System.Serializable]
public class DialogueMessage
{
    public int actorId;
    public string message;
}

[System.Serializable]
public class Actor
{
    public string name;
    public Sprite sprite;
}