using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueItem : MonoBehaviour
{
    //SCRIPT用途：開發中。管理Dialogue底下所有的UI元件。


    //DELEGATE


    //EVENT
    public event Action OnDialogueBoxShowed; //當對話框顯示
    public event Action OnDialoguePressed; //當玩家按下對話框
    


    //屬性宣告
    [SerializeField] private Transform container;
    [SerializeField] private GameObject background;

    private float animateSpeed = 0.5f;

    public Image actorImage;
    public TextMeshProUGUI actorName;
    public TextMeshProUGUI messageText;

    //OnEnable, OnDisable


    //Awake, Start, Update
    private void Awake()
    {
        background.GetComponent<CanvasGroup>().alpha = 0;
        container.localPosition = new Vector2(0, -Screen.height);
        background.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
    }


    //CORE FUNCTION
    private void DestroyPopup()
    {
        Destroy(gameObject);
    }

    public void OpenPopup()
    {
        gameObject.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();
        canvasGroup.LeanAlpha(1, animateSpeed);

        container.LeanMoveLocalY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;

        OnDialogueBoxShowed?.Invoke();
    }

    public void ClosePopup()
    {
        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();
        canvasGroup.LeanAlpha(0, animateSpeed);
        container.LeanMoveLocalY(-Screen.height, animateSpeed).setEaseInExpo().setOnComplete(OnComplete);
    }

    void OnComplete()
    {
        gameObject.SetActive(false);
        DestroyPopup();
    }

    public void SetDialogueText(string actorName, string message)
    {
        this.actorName.text = actorName;
        this.messageText.text = message;
    }


    //UNITY EDITOR FUNCTION
    public void DialoguePressed()
    {
        Debug.Log("pressed");
        OnDialoguePressed?.Invoke();
    }
}
