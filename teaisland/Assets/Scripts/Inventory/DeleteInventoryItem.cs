using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DeleteInventoryItem : MonoBehaviour
{
    public TextMeshProUGUI id;
    private int numberId;
    private PlayerInventory playerInventory;

    [SerializeField]
    private Button button_cross;

    private PlaySound playSound;

    private void Start()
    {
        playerInventory = GameObject.Find("Player").GetComponent<PlayerInventory>();

        playSound = gameObject.GetComponent<PlaySound>();

        if (SceneSingleton.Instance.getCurrentSceneName() == SceneSingleton.SceneList.Temple.ToString())
        {
            button_cross.interactable = false;
        }
    }

    public void DeleteItemByCross()
    {
        //SceneManager.GetActiveScene().name == "temple"
        if (SceneSingleton.Instance.getCurrentSceneName() == SceneSingleton.SceneList.Temple.ToString())
        {
            Debug.Log("不可以在此場景刪除物件");
            return;
        } else
        {
            StartCoroutine(delete());
        }
    }

    IEnumerator delete()
    {
        yield return new WaitWhile(() =>
        {
            return playSound.audioSource.isPlaying;
        });

        int.TryParse(id.text, out numberId);
        playerInventory.DeleteItemBy(numberId);

        //寫在這裡感覺可能不太對，但他現在有用
        Destroy(transform.parent.gameObject);
    }
}
