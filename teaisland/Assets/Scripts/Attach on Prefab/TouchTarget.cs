using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(GroundItem))]
public class TouchTarget : MonoBehaviour
{
    // SCRIPT用途：放置於Prefab上，用於判定用戶是否有選中此物件

    //public MeshRenderer renderer;
    

    //選中時的特效待追加
    private void OnMouseEnter()
    {
        //renderer.material.color = Color.red;
    }

    private void OnMouseExit()
    {
        //renderer.material.color = Color.white;
    }
}
