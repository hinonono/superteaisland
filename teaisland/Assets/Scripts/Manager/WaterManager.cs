using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterManager : MonoBehaviour
{
    //SCRIPT用途：管理Water的音效之類的

    private PlaySound playSound;

    private void OnEnable()
    {
        playSound = gameObject.GetComponent<PlaySound>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playSound.PlayClip();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {

        }
    }
}
