using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SelectTarget : MonoBehaviour
{
    //SCRIPT用途：發射射線並用於在3D世界中選擇物件
    //備註：可觸摸對象記得要裝touch target


    //DELEGATE


    //EVENT
    public event Action<GroundItem> onItemTouched; //當摸到可互動物件
    public event Action<GroundItem> onItemHold; //當長壓可互動物件
    public event Action onItemIntendToInteract; //想要和物件互動
    public event Action<GameObject> onPersonTouched; //當摸到可互動人物


    //屬性宣告
    [SerializeField] private Camera camera;
    private Player playerInput;
    public Vector2 safeAreaLR = new Vector2(400, 400);

    private float detectDistance = 5f;

    private int intentCount;
    private int intentCountMax = 3;


    //OnEnable, OnDisable
    private void OnEnable()
    {
        playerInput.SelectObject.Enable();
        playerInput.SelectObject.TaponObject.performed += DoTaponObject;
        playerInput.SelectObject.HoldonObject.performed += DoHoldonObject;
    }

    private void OnDisable()
    {
        playerInput.SelectObject.Disable();
        playerInput.SelectObject.TaponObject.performed -= DoTaponObject;
        playerInput.SelectObject.HoldonObject.performed -= DoHoldonObject;
    }


    //Awake, Start, Update
    private void Awake()
    {
        playerInput = new Player();
    }


    //CORE FUNCTION
    private void DoTaponObject(InputAction.CallbackContext context)
    {
        //之後要追加不要讓他按太快的程式
        if (GameObject.FindGameObjectWithTag("Popup"))
        {
            return;
        }

        Vector2 pos = playerInput.SelectObject.TouchPosition.ReadValue<Vector2>();
        bool isUsingJoystick = IsUsingJoystick(pos);

        Vector3 realPos = new Vector3(pos.x, pos.y, 0);
        Ray ray = camera.ScreenPointToRay(realPos);
        //String currentSceneName = SceneSingleton.Instance.getCurrentSceneName();


        if (isUsingJoystick)
        {
            return;
        }

        if (Physics.Raycast(ray, out RaycastHit hitInfo))
        {
            GameObject obj = hitInfo.collider.gameObject;
            if (hitInfo.distance <= detectDistance)
            {
                if (obj.GetComponent<TouchPerson>() != null)
                {
                    onPersonTouched?.Invoke(hitInfo.collider.gameObject);
                }
                else if (obj.GetComponent<TouchTarget>() != null)
                {
                    onItemTouched?.Invoke(hitInfo.collider.gameObject.GetComponent<GroundItem>());
                    intentCount += 1;
                }

                CheckIntentCount();
            }

            //if (currentSceneName == "Temple")
            //{
            //    if (hitInfo.collider.gameObject.GetComponent<TouchPerson>() != null)
            //    {
            //        if (hitInfo.distance <= detectDistance)
            //        {
            //            //觸碰到可互動的人物，推播event通知
            //            onPersonTouched?.Invoke(hitInfo.collider.gameObject);
            //            Debug.Log("你摸到人家了拉");
            //        }
            //    }
            //}
            //else
            //{
            //    if (hitInfo.collider.gameObject.GetComponent<TouchTarget>() != null)
            //    {
            //        if (hitInfo.distance <= detectDistance)
            //        {
            //            //觸碰到可互動的物件，推播event通知
            //            onItemTouched?.Invoke(hitInfo.collider.gameObject.GetComponent<GroundItem>());
            //            intentCount += 1;
            //        }

            //        if (intentCount >= 5)
            //        {
            //            onItemIntendToInteract?.Invoke();
            //            intentCount = 0;
            //        }
            //    }
            //}
        }
    }

    private void DoHoldonObject(InputAction.CallbackContext context)
    {

        if (GameObject.FindGameObjectWithTag("Popup"))
        {
            return;
        }

        Vector2 pos = playerInput.SelectObject.TouchPosition.ReadValue<Vector2>();
        bool isUsingJoystick = IsUsingJoystick(pos);
        Vector3 realPos = new Vector3(pos.x, pos.y, 0);
        Ray ray = camera.ScreenPointToRay(realPos);

        if (isUsingJoystick)
        {
            return;
        }

        if (Physics.Raycast(ray, out RaycastHit hitInfo))
        {
            GameObject obj = hitInfo.collider.gameObject;
            if (hitInfo.distance <= detectDistance)
            {
                if (obj.GetComponent<TouchTarget>() != null)
                {
                    onItemHold?.Invoke(hitInfo.collider.gameObject.GetComponent<GroundItem>());
                } else
                {
                    intentCount++;
                }

                CheckIntentCount();
            }

            //if (hitInfo.collider.gameObject.GetComponent<TouchTarget>() != null)
            //{
            //    if (hitInfo.distance <= detectDistance)
            //    {
            //        onItemHold?.Invoke(hitInfo.collider.gameObject.GetComponent<GroundItem>());
            //    }
            //    else
            //    {
            //        intentCount += 1;
            //    }

            //    if (intentCount >= intentCountMax)
            //    {
            //        onItemIntendToInteract?.Invoke();
            //        intentCount = 0;
            //    }
            //}
        }
    }

    private bool IsUsingJoystick(Vector2 pos)
    {
        //用途：用於防止操作joystick時觸發採集物件
        int safeAreaWidth = 400;
        int safeAreaHeight = 400;

        if (0 <= pos.x && pos.x <= safeAreaWidth || Screen.width - safeAreaWidth <= pos.x && pos.x <= Screen.width)
        {
            if (0 <= pos.y && pos.y <= safeAreaHeight)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    private void CheckIntentCount()
    {
        if (intentCount >= 5)
        {
            onItemIntendToInteract?.Invoke();
            intentCount = 0;
        }
    }


    //UNITY EDITOR FUNCTION
}
