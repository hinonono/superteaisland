using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

[RequireComponent(typeof(NavMeshAgent))]
public class DuckMotionController : MonoBehaviour
{
    //SCRIPT用途：控制像鴨子一樣，只需移動的動作
    public Transform[] waypoints;

    private bool hasWaypoints;
    private NavMeshAgent agent;
    private int waypointIndex;
    private Vector3 target;

    private void Awake()
    {
        if (waypoints.Length != 0)
        {
            hasWaypoints = true;
        }
        else
        {
            hasWaypoints = false;
        }
    }

    private void Start()
    {
        agent = gameObject.GetComponent<NavMeshAgent>();

        UpdateDestination();
    }

    private void Update()
    {
        if (Vector3.Distance(transform.position, target) < 1)
        {
            IterateWaypointIndex();
            UpdateDestination();
        }
    }

    private void UpdateDestination()
    {
        if (hasWaypoints)
        {
            target = waypoints[waypointIndex].position;
            agent.SetDestination(target);
        }
    }

    private void IterateWaypointIndex()
    {
        if (hasWaypoints)
        {
            waypointIndex++;
            if (waypointIndex == waypoints.Length)
            {
                waypointIndex = 0;
            }
        }
    }
}
