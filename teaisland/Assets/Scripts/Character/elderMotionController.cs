using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ElderMotionController : MonoBehaviour
{
    //SCRIPT用途：控制老人的動作

    private Animator animator;
    private enum AnimationList
    {
        //使用enum列舉避免呼叫動畫名稱時出現錯誤
        walk
    }

    public Transform[] waypoints;

    private bool hasWaypoints;
    private NavMeshAgent agent;
    private int waypointIndex;
    private Vector3 target;

    private void Awake()
    {
        if (waypoints.Length != 0)
        {
            hasWaypoints = true;
        } else
        {
            hasWaypoints = false;
        }
    }

    private void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        agent = gameObject.GetComponent<NavMeshAgent>();

        UpdateDestination();

        animator.SetBool(AnimationList.walk.ToString(), true);
    }

    private void Update()
    {
        if (Vector3.Distance(transform.position, target) < 1)
        {
            IterateWaypointIndex();
            UpdateDestination();
        }
    }

    private void UpdateDestination()
    {
        if (hasWaypoints)
        {
            target = waypoints[waypointIndex].position;
            agent.SetDestination(target);
        }
    }

    private void IterateWaypointIndex()
    {
        if (hasWaypoints)
        {
            waypointIndex++;
            if (waypointIndex == waypoints.Length)
            {
                waypointIndex = 0;
            }
        }
    }
}
