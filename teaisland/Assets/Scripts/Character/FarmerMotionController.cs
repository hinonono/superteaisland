using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FarmerMotionController : MonoBehaviour
{
    //SCRIPT用途：控制茶農的動作

    private Animator animator;
    private enum AnimationList
    {
        //使用enum列舉避免呼叫動畫名稱時出現錯誤
        picktea
    }

    public Transform[] waypoints;

    private bool picktea;
    private bool hasWaypoints;
    private NavMeshAgent agent;
    private int waypointIndex;
    private Vector3 target;

    private float delay;

    private void Awake()
    {
        if (waypoints.Length != 0)
        {
            hasWaypoints = true;
        }
        else
        {
            hasWaypoints = false;
        }
    }

    private void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        agent = gameObject.GetComponent<NavMeshAgent>();
        picktea = false;

        delay = Random.Range(0, 5);

        UpdateDestination();
        StartPickTea();

        //animator.SetBool(AnimationList.picktea.ToString(), true);
    }

    private void Update()
    {
        if (Vector3.Distance(transform.position, target) < 1)
        {
            IterateWaypointIndex();
            UpdateDestination();
        }
    }

    private void UpdateDestination()
    {
        if (hasWaypoints)
        {
            target = waypoints[waypointIndex].position;
            agent.SetDestination(target);
        }
    }

    private void IterateWaypointIndex()
    {
        if (hasWaypoints)
        {
            waypointIndex++;
            if (waypointIndex == waypoints.Length)
            {
                waypointIndex = 0;
            }
        }
    }

    private void StartPickTea()
    {
        //會引發Crash 我也不知道為什麼
        StartCoroutine(pickTheTea(delay));
    }

    IEnumerator pickTheTea(float delay)
    {
        yield return new WaitForSeconds(delay);
        while (true)
        {
            if (picktea)
            {
                animator.SetBool(AnimationList.picktea.ToString(), false);
                picktea = false;
                yield return new WaitForSeconds(5);
            }
            else
            {
                animator.SetBool(AnimationList.picktea.ToString(), true);
                picktea = true;
                yield return new WaitForSeconds(5);
            }
        }
    }
}
