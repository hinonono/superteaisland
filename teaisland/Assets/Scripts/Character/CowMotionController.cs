using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CowMotionController : MonoBehaviour
{
    //SCRIPT用途：


    //DELEGATE


    //EVENT


    //屬性宣告
    private Animator animator;
    private enum AnimationList
    {
        //使用enum列舉避免呼叫動畫名稱時出現錯誤
        eat
    }

    public Transform[] waypoints;

    private bool eat;
    private bool hasWaypoints;
    private NavMeshAgent agent;
    private int waypointIndex;
    private Vector3 target;

    private float delay;


    //OnEnable, OnDisable


    //Awake, Start, Update
    private void Awake()
    {
        if (waypoints.Length != 0)
        {
            hasWaypoints = true;
        }
        else
        {
            hasWaypoints = false;
        }
    }

    private void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        agent = gameObject.GetComponent<NavMeshAgent>();
        eat = false;

        delay = Random.Range(0, 5);

        UpdateDestination();
        StartPickTea();

        //animator.SetBool(AnimationList.picktea.ToString(), true);
    }

    private void Update()
    {
        if (Vector3.Distance(transform.position, target) < 1)
        {
            IterateWaypointIndex();
            UpdateDestination();
        }
    }

    private void UpdateDestination()
    {
        if (hasWaypoints)
        {
            target = waypoints[waypointIndex].position;
            agent.SetDestination(target);
        }
    }

    private void IterateWaypointIndex()
    {
        if (hasWaypoints)
        {
            waypointIndex++;
            if (waypointIndex == waypoints.Length)
            {
                waypointIndex = 0;
            }
        }
    }

    private void StartPickTea()
    {
        //會引發Crash 我也不知道為什麼
        StartCoroutine(pickTheTea(delay));
    }

    IEnumerator pickTheTea(float delay)
    {
        yield return new WaitForSeconds(delay);
        while (true)
        {
            if (eat)
            {
                animator.SetBool(AnimationList.eat.ToString(), false);
                eat = false;
                yield return new WaitForSeconds(1);
            }
            else
            {
                animator.SetBool(AnimationList.eat.ToString(), true);
                eat = true;
                yield return new WaitForSeconds(1);
            }
        }
    }

    //CORE FUNCTION


    //UNITY EDITOR FUNCTION
}
