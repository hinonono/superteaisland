using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FollowPlayer : MonoBehaviour
{
    //SCRIPT用途：使特定物件跟隨Player


    //DELEGATE


    //EVENT


    //屬性宣告
    private GameObject follower;
    private GameObject target;


    //OnEnable, OnDisable


    //Awake, Start, Update
    private void Start()
    {
        follower = gameObject;
        StartCoroutine(CheckSceneExist());
    }

    private void Update()
    {
        
        if (target != null)
        {
            Vector3 newPosition = new Vector3(
            target.transform.position.x,
            follower.transform.position.y,
            target.transform.position.z);
            follower.transform.position = newPosition;
        }
    }


    //CORE FUNCTION
    IEnumerator CheckSceneExist()
    {
        //檢查場景是否已經被載入
        Scene additiveScene = SceneManager.GetSceneByName("CANVAS_GAMEORDER");
        while (!additiveScene.isLoaded)
        {
            
            yield return null;
            Debug.Log(additiveScene.name);
            target = GameObject.Find("Player");
        }
    }


    //UNITY EDITOR FUNCTION
}
