using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSingleton : MonoBehaviour
{
    //Singleton 設定START
    private static SceneSingleton _instance;

    public static SceneSingleton Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject gameObj = new GameObject("Scene Singleton");
                gameObj.AddComponent<SceneSingleton>();
            }

            return _instance;
        }
    }
    //Singleton 設定END

    public enum SceneList
    {
        OpeningVideo,
        QrForExhibit,
        Select_map,
        Map_red,
        Map_yellow,
        Map_cyan,
        Map_white,
        Map_black,
        Map_green,
        NameTeaCard,
        Temple,
        Tap_effect,
        Home,
        AboutTaiocha,
        Quick_tea,
        Quick_black,
        Quick_red,
        Quick_yellow,
        Quick_cyan,
        Quick_white,
        Quick_green,
        Quick_pay,
        Quick_cash,
        Quick_linePay,
        TeachTaio,
        TeachTaio1,
        TeachTaio2,
        TeachTaio3,
        TeachTaio4,
        TeachTaio5
    }

    public string destination;

    public void EmptyFuncion()
    {

    }

    public string getCurrentDestination()
    {
        return destination;
    }

    public string getCurrentSceneName()
    {
        return SceneManager.GetActiveScene().name;
    }

    public void LoadSceneByDestination(LoadSceneMode mode = LoadSceneMode.Single)
    {
        if (destination != null)
        {
            SceneManager.LoadScene(destination, mode);
        }
    }

    public IEnumerator LoadMyScene(string des, LoadSceneMode mode)
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(des, mode);
        yield return async;
    }

    void Awake()
    {
        _instance = this;
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        //初始化你的數值（如果有需要的話）
    }
}
