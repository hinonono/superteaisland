using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowConsole : MonoBehaviour
{
    //SCRIPT用途：


    //DELEGATE


    //EVENT


    //屬性宣告
    private Player inputs;


    //OnEnable, OnDisable
    private void OnEnable()
    {
        inputs.PlayerMain.Console.Enable();
        inputs.PlayerMain.Console.performed += CreateConsole;
    }

    private void OnDisable()
    {
        inputs.PlayerMain.Console.Disable();
    }


    //Awake, Start, Update
    private void Awake()
    {
        inputs = new Player();
        DontDestroyOnLoad(gameObject);
    }


    //CORE FUNCTION
    private void CreateConsole(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        if (GameObject.Find("Console") == null)
        {
            GameObject gameObj = new GameObject("Console");
            gameObj.AddComponent<ConsoleToGUI>();
            DontDestroyOnLoad(gameObj);
        } else
        {
            GameObject gameObj = GameObject.Find("Console");
            Destroy(gameObj);
        }
    }


    //UNITY EDITOR FUNCTION
}
