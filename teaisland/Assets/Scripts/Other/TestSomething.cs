using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class TestSomething : MonoBehaviour
{
    //SCRIPT用途：測試某部分是否正常（不用的時候要記得從component刪除）

    private void OnEnable()
    {
        Debug.LogWarning("一項附加於 " + gameObject.name + " 的測試正在執行中。不用時記得移除。");
    }
}
