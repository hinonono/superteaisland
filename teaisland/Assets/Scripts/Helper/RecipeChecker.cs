using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecipeChecker : MonoBehaviour
{
    //SCRIPT用途：在命名茶卡場景中檢查玩家的inventory和recipe是否有符合


    //DELEGATE
    public delegate void withString(string name);


    //EVENT
    public event withString OnCheckCompleted;


    //屬性宣告
    [SerializeField] private RecipeDatabaseObject recipeDatabase;
    [SerializeField] private InventoryObject inventory;

    private GUIManager_NameTeaCard uiManager;

    private List<Item> teaPlayerHave;
    private List<Item> toppingPlayerHave;
    private List<int> idToBeDeleted;

    private List<Recipe> recipes_red;
    private List<Recipe> recipes_green;
    private List<Recipe> recipes_yellow;
    private List<Recipe> recipes_black;
    private List<Recipe> recipes_white;
    private List<Recipe> recipes_cyan;

    private List<ItemObject> results;
    private ItemObject result;


    //OnEnable, OnDisable


    //Awake, Start, Update
    private void Start()
    {
        teaPlayerHave = new List<Item>();
        toppingPlayerHave = new List<Item>();
        idToBeDeleted = new List<int>();

        recipes_red = new List<Recipe>();
        recipes_green = new List<Recipe>();
        recipes_yellow = new List<Recipe>();
        recipes_black = new List<Recipe>();
        recipes_white = new List<Recipe>();
        recipes_cyan = new List<Recipe>();

        results = new List<ItemObject>();

        //uiManager = GameObject.Find("Ui Manager").GetComponent<GUIManager_NameTeaCard>();
        //uiManager.OnValueChanged += UiManager_OnValueChanged;

        CategorizeRecipe();
        CategorizeInventory();
        CompareRecipe();


        //showTiaochaResult();
    }


    //CORE FUNCTION
    private void CategorizeRecipe()
    {
        for (int i = 0; i < recipeDatabase.recipes.Length; i++)
        {
            Recipe recipe = recipeDatabase.recipes[i];

            switch (recipe.type)
            {
                case Recipe.recipeType.TeaRed:
                    recipes_red.Add(recipe);
                    break;

                case Recipe.recipeType.TeaGreen:
                    recipes_green.Add(recipe);
                    break;

                case Recipe.recipeType.TeaBlack:
                    recipes_black.Add(recipe);
                    break;

                case Recipe.recipeType.TeaCyan:
                    recipes_cyan.Add(recipe);
                    break;

                case Recipe.recipeType.TeaWhite:
                    recipes_white.Add(recipe);
                    break;

                case Recipe.recipeType.TeaYellow:
                    recipes_yellow.Add(recipe);
                    break;

                default:
                    Debug.LogError("食譜分類錯誤！");
                    break;
            }
        }

        if (recipes_red == null)
        {
            Debug.LogWarning("資料庫裡沒有紅茶的配方！");
        }

        if (recipes_black == null)
        {
            Debug.LogWarning("資料庫裡沒有黑茶的配方！");
        }

        if (recipes_cyan == null)
        {
            Debug.LogWarning("資料庫裡沒有青茶的配方！");
        }

        if (recipes_green == null)
        {
            Debug.LogWarning("資料庫裡沒有綠茶的配方！");
        }

        if (recipes_white == null)
        {
            Debug.LogWarning("資料庫裡沒有白茶的配方！");
        }

        if (recipes_yellow == null)
        {
            Debug.LogWarning("資料庫裡沒有黃茶的配方！");
        }
    }

    private void CategorizeInventory()
    {
        //VOID用途：將玩家擁有的item 依照tea, topping分類至對應變數中

        for (int i = 0; i < inventory.Container.Items.Count; i++)
        {
            Item item = inventory.Container.Items[i].item;

            if (item.type == ItemType.Tea)
            {
                teaPlayerHave.Add(item);
            }
            else if (item.type == ItemType.Topping)
            {
                toppingPlayerHave.Add(item);
            }
        }
    }

    private void CompareRecipe()
    {
        switch (teaPlayerHave[0].Id)
        {
            case 0:

                if (recipes_red.Count != 0)
                {
                    compareToppingAndRecipe(recipes_red);
                } else
                {
                    Debug.LogError("由於缺乏紅茶的配方，因此無法進行比較");
                }
                break;

            case 1:
                if (recipes_green.Count != 0)
                {
                    compareToppingAndRecipe(recipes_green);
                } else
                {
                    Debug.LogError("由於缺乏綠茶的配方，因此無法進行比較");
                }
                
                break;

            case 2:
                if (recipes_black.Count != 0)
                {
                    compareToppingAndRecipe(recipes_black);
                } else
                {
                    Debug.LogError("由於缺乏黑茶的配方，因此無法進行比較");
                }
                
                break;

            case 3:
                if (recipes_cyan.Count != 0)
                {
                    compareToppingAndRecipe(recipes_cyan);
                } else
                {
                    Debug.LogError("由於缺乏青茶的配方，因此無法進行比較");
                }
                break;

            case 4:
                if (recipes_white.Count != 0)
                {
                    compareToppingAndRecipe(recipes_white);
                } else
                {
                    Debug.LogError("由於缺乏白茶的配方，因此無法進行比較");
                }
                
                break;

            case 5:
                if (recipes_yellow.Count != 0)
                {
                    compareToppingAndRecipe(recipes_yellow);
                } else
                {
                    Debug.LogError("由於缺乏黃茶的配方，因此無法進行比較");
                }
                
                break;

            default:
                Debug.LogError("無法將玩家擁有的茶葉對應至六大茶系中");
                break;
        }
    }

    private void compareToppingAndRecipe(List<Recipe> recipe)
    {
        //FUNC用途：比較玩家擁有的配料 和 配方中記載的配料 是否符合
        //如果符合就將結果加入列表中

        for (int i = 0; i < toppingPlayerHave.Count; i++)
        {
            for (int j = 0; j < recipe.Count; j++)
            {
                if (toppingPlayerHave[i].Id == recipe[j].topping.Id)
                {
                    results.Add(recipe[j].getResult());
                }
            }
        }

        if (results.Count != 0)
        {
            int num = UnityEngine.Random.Range(0, results.Count);
            result = results[num];
            OnCheckCompleted?.Invoke(result.itemName);
        }
    }

    //private void showTiaochaResult()
    //{
    //    if (results.Count != 0)
    //    {
            
    //        TiaochaObject t = result as TiaochaObject;

    //        inventory.AddItem(new Item(result), 1);
    //        inventory.AddItemToTiaochaContainer(new Item(t), 1);

    //        for (int i = 0; i < inventory.Container.Items.Count; i++)
    //        {

    //            Item item = inventory.Container.Items[i].item;

    //            if (item.type == ItemType.Tiaocha)
    //            {
    //                //tiaochaPlayerHave.Add(item);
    //            }
    //            else
    //            {
    //                idToBeDeleted.Add(item.Id);
    //            }
    //        }

    //        foreach (var id in idToBeDeleted)
    //        {
    //            inventory.DeleteItem(id);
    //        }
    //    } else
    //    {
    //        Debug.LogError("結果列表中沒有內容！");
    //    }
    //}

    public void AddTiaocha(string customName)
    {
        //傳入不同類別，以套用不同的constructor
        //Item item_item = new Item(result);
        Item item_tiaocha = new Item((TiaochaObject)result);
        item_tiaocha.customName = customName;

        //inventory.AddItem(item_item, 1);
        inventory.AddItemToTiaochaContainer(item_tiaocha, 1);

        for (int i = 0; i < inventory.Container.Items.Count; i++)
        {

            Item item = inventory.Container.Items[i].item;

            if (item.type != ItemType.Tiaocha)
            {
                idToBeDeleted.Add(item.Id);
            }
        }

        foreach (var id in idToBeDeleted)
        {
            inventory.DeleteItem(id);
        }
    }
}
