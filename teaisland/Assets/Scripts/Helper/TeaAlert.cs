using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;
using UnityEngine.SceneManagement;

public class TeaAlert : MonoBehaviour
{
    //DEPRECATED
    //控制Alert出現與消失的速度
    private float animateSpeed = 0.5f;

    private GameObject[] walls;
    private GameObject[] hintAreas;
    private TextMeshProUGUI alertText;
    [SerializeField]
    private bool findHintArea;

    private PlayerInventory playerInventory;

    [SerializeField]
    private SelectTarget selectTarget;

    private void Start()
    {
        //設定Alert 的起始位置
        gameObject.transform.localPosition = new Vector2(0, Screen.height);
        alertText = GetComponentInChildren<TextMeshProUGUI>();

        subscribeToHintArea();
        subscribeToWall();

        playerInventory = GameObject.Find("Player").GetComponent<PlayerInventory>();
        playerInventory.onItemPicked += PlayerInventory_onItemPicked;


        selectTarget.onItemIntendToInteract += SelectTarget_onItemIntendToInteract;

        showFirstMessageWhenSceneLoaded();
    }

    private void subscribeToHintArea()
    {
        //VOID用途：訂閱Hint Area

        if (findHintArea)
        {
            Debug.Log("Tea Alert will search for HintArea in the scene now.");
            hintAreas = GameObject.FindGameObjectsWithTag("HintArea");
            if (hintAreas == null)
            {
                Debug.Log("hintAreas array is empty");
            }
            else
            {
                for (int i = 0; i < hintAreas.Length; i++)
                {
                    hintAreas[i].GetComponent<HintArea>().onHintAreaTouched += HintArea_onHintAreaTouched;
                }
            }
        }
    }

    private void subscribeToWall()
    {
        //VOID用途：訂閱牆壁的事件

        walls = GameObject.FindGameObjectsWithTag("Wall");
        for (int i = 0; i < walls.Length; i++)
        {
            walls[i].GetComponent<WallDetecter>().onWallTouched += WallDetecter_onWallTouched;
        }
    }

    private void showFirstMessageWhenSceneLoaded()
    {
        if (SceneSingleton.Instance.getCurrentSceneName() == SceneSingleton.SceneList.Temple.ToString())
        {
            openAlert("請前往祭壇上方去拜見長老", 5);
        } else
        {
            openAlert("收集至少一種茶葉與配料去拜見長老吧！", 5);
        }
    }

    private void SelectTarget_onItemIntendToInteract()
    {
        openAlert("你需要靠近一點才能進行互動");
    }

    private void WallDetecter_onWallTouched(object sender, EventArgs e)
    {
        string[] texts =
        {
            "似乎有股神秘的力量阻擋我們不能夠再往前進...",
            "前方似乎沒有路了，換個方向前進試試看吧！"
        };

        string text = texts[Random.Range(0, texts.Length)];
        openAlert(text);
    }

    private void HintArea_onHintAreaTouched(string text)
    {
        openAlert(text);
    }

    private void PlayerInventory_onItemPicked(string itemName, int quantity)
    {
        string text = "一份 " + itemName + " 已放入你的材料庫中！";
        openAlert(text);
    }

    public void openAlert(string text, int sec = 3)
    {
        alertText.text = text;
        gameObject.SetActive(true);
        gameObject.transform.localPosition = new Vector2(0, Screen.height);
        gameObject.LeanMoveLocalY(780, animateSpeed).setEaseOutExpo().delay = 0.1f;

        StartCoroutine(fadeOutAlert(sec));
    }

    IEnumerator fadeOutAlert(int sec)
    {
        yield return new WaitForSeconds(sec);
        closeAlert();
    }

    private void closeAlert()
    {
        gameObject.LeanMoveLocalY(Screen.height, animateSpeed).setEaseInSine().setOnComplete(onComplete);
    }

    void onComplete()
    {
        gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        if (findHintArea)
        {
            for (int i = 0; i < hintAreas.Length; i++)
            {
                hintAreas[i].GetComponent<HintArea>().onHintAreaTouched -= HintArea_onHintAreaTouched;
            }
        }

    }
}
