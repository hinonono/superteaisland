using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering.PostProcessing;

[RequireComponent(typeof(BoxCollider))]
public class ChangePostProcessProfile : MonoBehaviour
{
    //SCRIPT用途：切換Post Processing Profile
    //常用於水下場景顯示

    [SerializeField]
    private VolumeProfile normal, water;

    private Volume volume;

    private void Start()
    {
        volume = FindObjectOfType<Volume>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            volume.profile = water;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            volume.profile = normal;
        }
    }

}
