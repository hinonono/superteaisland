using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldEffectInstance : MonoBehaviour
{
    private Animator animator;
    private float delay;
    private Player inputs;
    private bool isInputStart = false;

    public event Action OnStop;

    private void Awake()
    {
        inputs = new Player();

        inputs.SelectObject.HoldonObject.Enable();
        //inputs.SelectObject.HoldonObject.started += IsInputStart;

        isInputStart = true;
    }

    private void Start()
    {
        animator = this.GetComponent<Animator>();
        animator.SetBool("stop", false);

        delay = 0f;
    }

    private void Update()
    {
        if (animator.GetBool("stop") == true)
        {
            OnStop?.Invoke();
        }
    }

    public void SetProperty(string name, bool status)
    {
        animator.SetBool(name, status);
    }
}
