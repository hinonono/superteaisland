using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class HoldEffect : MonoBehaviour
{
    public GameObject instance;
    private Player inputs;

    private GameObject instantiatedInstance;
    private bool isHoldStart = false;

    private void Awake()
    {
        inputs = new Player();
    }

    private void OnEnable()
    {
        inputs.SelectObject.Enable();
        inputs.Touch.Enable();

        inputs.SelectObject.HoldonObject.performed += DoHold;
    }

    private void DoHold(InputAction.CallbackContext context)
    {
        if (!GameObject.Find("Hold Effect(Clone)"))
        {
            Vector2 pos = inputs.Touch.PrimaryPosition.ReadValue<Vector2>();

            instantiatedInstance = Instantiate(instance, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
            instantiatedInstance.transform.GetComponent<RectTransform>().position = pos;

            instantiatedInstance.transform.parent = gameObject.transform;
            instantiatedInstance.GetComponent<HoldEffectInstance>().OnStop += HoldEffect_OnStop;

            isHoldStart = true;
        }
    }

    private void HoldEffect_OnStop()
    {
        Destroy(instantiatedInstance);
    }

    private void Update()
    {
        if (instantiatedInstance != null)
        {
            if (isHoldStart == true && inputs.SelectObject.HoldonObject.inProgress == false)
            {
                instantiatedInstance.GetComponent<HoldEffectInstance>().SetProperty("stop", true);
            }
        }
    }


    private void OnDisable()
    {
        inputs.PlayerMain.Disable();
    }
}
