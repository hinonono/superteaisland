using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TapEffectInstance : MonoBehaviour
{

    private Animator animator;
    private float delay;

    private void Start()
    {
        animator = this.GetComponent<Animator>();
        animator.SetBool("Start", true);

        delay = 1f;

        animator.SetBool("Start", false);

        Destroy(gameObject, animator.GetCurrentAnimatorStateInfo(0).length + delay);
    }
}
