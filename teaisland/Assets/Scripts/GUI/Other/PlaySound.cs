using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlaySound : MonoBehaviour
{
    
    public AudioSource audioSource;
    [SerializeField] private AudioClip clip;

    private void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    public void PlayClip(bool loop = false)
    {
        audioSource.loop = loop;
        audioSource.clip = this.clip;
        audioSource.Play();
    }

    public void PlayWithSpecificClip(AudioClip clip, bool loop = false)
    {
        audioSource.clip = clip;
        audioSource.loop = loop;

        if (!audioSource.isPlaying)
        {
            audioSource.Play();
        }
    }

    public void StopClip()
    {
        audioSource.Stop();
    }
}
