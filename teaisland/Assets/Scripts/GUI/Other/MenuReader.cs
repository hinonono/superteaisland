using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuReader : MonoBehaviour
{
    //SCRIPT用途：負責處理選地圖頁面的資料from JSON
    [System.Serializable]
    public class UInfo
    {
        public string flavor;
        public string effect;
        public string[] coordination;
    }

    [System.Serializable]
    public class UITopping
    {
        public string name;
        public string imagepath;

        public UInfo uinfo;
    }

    [System.Serializable]
    public class UItea
    {
        public string name;
        public int id;
        public string imagepath;
        public string areaname;
        public UInfo uinfo;
        public UITopping[] uitopping;
    }

    [System.Serializable]
    public class UIteaList
    {
        public UItea[] uitea;
    }

    //宣告變數部分
    public UIteaList myuiteaList = new UIteaList();
    

    private void OnEnable()
    {
        TextAsset file = Resources.Load("menu", typeof(TextAsset)) as TextAsset;
        myuiteaList = JsonUtility.FromJson<UIteaList>(file.text);
    }
}
