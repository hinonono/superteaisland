using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TapEffect : MonoBehaviour
{
    private static TapEffect tapEffect;

    public GameObject instance;
    private Player inputs;

    private void Awake()
    {
        if (tapEffect != null)
        {
            Destroy(gameObject);
        }
        else
        {
            tapEffect = this;
            DontDestroyOnLoad(gameObject);
            inputs = new Player();
        }
    }

    private void OnEnable()
    {
        inputs.SelectObject.Enable();
        inputs.Touch.Enable();

        inputs.SelectObject.TaponObject.performed += DoTap;
    }

    private void DoTap(InputAction.CallbackContext context)
    {
        Vector2 pos = inputs.Touch.PrimaryPosition.ReadValue<Vector2>();

        var _instance = Instantiate(instance, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.Euler(0, 0, 0));
        _instance.transform.GetComponent<RectTransform>().position = pos;


        _instance.transform.parent = gameObject.transform;
    }


    private void OnDisable()
    {
        //inputs.PlayerMain.Disable();
    }
}
