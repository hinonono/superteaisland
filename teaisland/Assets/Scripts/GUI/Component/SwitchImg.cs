using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchImg : MonoBehaviour
{
    public GameObject[] sence_img;
    int index;
    void Start()
    {
        index = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(index >= 3)
        index = 3;

        if(index < 0)
        index = 0;
        if(index == 0)
        {
            sence_img[0].gameObject.SetActive(true);
        }
    }

    public void Next()
    {
        index += 1;
        for(int i = 0; i < sence_img.Length; i++)
        {
            sence_img[i].gameObject.SetActive(false);
            sence_img[index].gameObject.SetActive(true);
        }
        Debug.Log(index);
    }
    public void Previous()
    {
        index -= 1;
        for(int i = 0; i < sence_img.Length; i++)
        {
            sence_img[i].gameObject.SetActive(false);
            sence_img[index].gameObject.SetActive(true);
        }
        Debug.Log(index);
    }
}
