using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GAlert : MonoBehaviour
{
    //SCRIPT用途：開發中。用於取代Tea Alert。


    //DELEGATE


    //EVENT
    public event Action OnAlertShowed;
    public event Action OnAlertDismissed;


    //屬性宣告
    [SerializeField] private TextMeshProUGUI messageGameObj;
    private float animateSpeed = 0.5f;

    public string message;


    //OnEnable, OnDisable


    //Awake, Start, Update
    private void Awake()
    {
        message = "Default Message";
        SetAlertText(message);

        gameObject.transform.localPosition = new Vector2(0, Screen.height);
    }


    //CORE FUNCTION
    private void SetAlertText(string message)
    {
        messageGameObj.text = message;
    }

    public void ShowAlert(string message, int sec = 3)
    {
        messageGameObj.text = message;

        gameObject.transform.localPosition = new Vector2(0, Screen.height);
        gameObject.LeanMoveLocalY(780, animateSpeed).setEaseOutExpo().delay = 0.1f;

        StartCoroutine(fadeOutAlert(sec));
    }

    IEnumerator fadeOutAlert(int sec)
    {
        yield return new WaitForSeconds(sec);
        DismissAlert();
    }

    public void DismissAlert()
    {
        gameObject.LeanMoveLocalY(Screen.height, animateSpeed).setEaseInSine().setOnComplete(OnComplete);
    }

    void OnComplete()
    {
        gameObject.SetActive(false);
        Destroy(gameObject);
    }


    //UNITY EDITOR FUNCTION
}
