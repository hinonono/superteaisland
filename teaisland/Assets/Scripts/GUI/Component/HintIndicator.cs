using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HintIndicator : MonoBehaviour
{
    //SCRIPT用途：開發中。用於茶葉與配料的指示顯示。


    //DELEGATE


    //EVENT


    //屬性宣告
    private Image image;
    public enum HintType
    {
        Topping,
        Tea
    }

    public HintType type;


    //OnEnable, OnDisable


    //Awake, Start, Update
    private void Awake()
    {
        image = GetComponent<Image>();
    }

    private void Start()
    {
        switch (type)
        {
            case HintType.Topping:
                image.sprite = Resources.Load("GUI/Icon/hint_topping", typeof(Sprite)) as Sprite;

                break;
            case HintType.Tea:
                image.sprite = Resources.Load("GUI/Icon/hint_tea", typeof(Sprite)) as Sprite;
                break;
            default:
                break;
        }
    }

    private void Update()
    {
        gameObject.transform.Rotate(0f, 1f, 0f);
    }


    //CORE FUNCTION


    //UNITY EDITOR FUNCTION
}
