using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using static GPopup;

public class CartPopup : MonoBehaviour
{
    //SCRIPT用途：控制購物車彈窗


    //DELEGATE


    //EVENT
    public event Action OnButtonPrimaryPressed;
    public event Action OnButtonSecondaryPressed;

    //屬性宣告
    [SerializeField] private RectTransform container;
    [SerializeField] private CanvasGroup background;
    [SerializeField] private TextMeshProUGUI text_quantity;
    [SerializeField] private TextMeshProUGUI text_total_price;
    [SerializeField] private GameObject scroll_content;
    private List<GameObject> cartPopupItems = new List<GameObject>();
    private List<GameObject> objectToDelete = new List<GameObject>();

    private float animateSpeed = 0.5f;

    public GButton gButton_primary;
    public GButton gButton_secondary;

    public int quantity;
    public int totalPrice;


    //OnEnable, OnDisable
    private void OnEnable()
    {
        quantity = 0;
        totalPrice = 0;
    }


    //Awake, Start, Update
    private void Awake()
    {
        gButton_primary.SetButtonText("送出訂單");
        gButton_secondary.SetButtonText("繼續點餐");
    }


    //CORE FUNCTION
    public void OpenPopup()
    {
        text_quantity.text = quantity.ToString();
        text_total_price.text = totalPrice.ToString();

        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();


        //設定自身大小，並將自身的位置歸零
        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        gameObject.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;


        //設定背景大小，並將透明度設為0，並用動畫表示顯示過程
        background.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
        canvasGroup.alpha = 0;
        canvasGroup.LeanAlpha(1, animateSpeed);


        //將彈窗置於低「一個螢幕高」的位置，並用動畫表示歸零過程
        container.anchoredPosition = new Vector2(0, -Screen.height);
        container.LeanMoveY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;
    }

    private void DestroyPopup()
    {
        Destroy(gameObject);
    }

    public void ClosePopup()
    {
        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();
        canvasGroup.LeanAlpha(0, animateSpeed);
        container.LeanMoveLocalY(-Screen.height, animateSpeed).setEaseInExpo().setOnComplete(OnComplete);
    }

    void OnComplete()
    {
        gameObject.SetActive(false);
        DestroyPopup();
    }

    public void InstantiateCartItem(InventoryObject inventory)
    {
        //用途：生成購物車裡頭的cell item

        foreach (var item in inventory.TiaochaContainer.Items)
        {
            GameObject cartItem = Instantiate(Resources.Load("Prefab/CartItem", typeof(GameObject)) as GameObject);
            cartItem.transform.parent = scroll_content.transform;

            CartItem cartItemInstance = new CartItem(item.item.customName, item.item.fromTea, item.item.fromTopping, item.item.guid);

            cartItem.GetComponent<CartPopupItem>().SetCartPopupItem(cartItemInstance);
            cartItem.GetComponent<CartPopupItem>().OnDeleteButtonPressed += DeleteItemById;
            cartPopupItems.Add(cartItem);
        }

        UpdatePriceAndQuantity();
    }

    public void UpdatePriceAndQuantity()
    {
        int quantity = cartPopupItems.Count;
        int price = 180 * quantity;

        text_quantity.text = quantity.ToString();
        text_total_price.text = price.ToString();
    }

    public void UpdateCartItem()
    {
        Debug.Log("Call from UpdateCartItem");
    }

    public void DeleteItemById(Guid guid)
    {
        Debug.Log("Call from DeleteItemById");

        Inventory tiaochaContainer = gameObject.GetComponentInParent<GUIManager_Gameorder>().playerInventory.inventory.TiaochaContainer;
        

        foreach (var item in cartPopupItems)
        {
            if (item.GetComponent<CartPopupItem>().id == guid)
            {
                objectToDelete.Add(item);
                Destroy(item);
                
            }           
        }

        for (int i = 0; i < tiaochaContainer.Items.Count; i++)
        {
            for (int j = 0; j < objectToDelete.Count; j++)
            {
                if (objectToDelete[j].GetComponent<CartPopupItem>().id == tiaochaContainer.Items[i].item.guid)
                {
                    tiaochaContainer.Items.Remove(tiaochaContainer.Items[i]);
                }
            }
        }

        foreach (var item in objectToDelete)
        {
            cartPopupItems.Remove(item);
        }

        Debug.Log("cartPopupItems.Count" + cartPopupItems.Count);

        if (cartPopupItems.Count == 0)
        {
            string title = "缺乏調茶";
            string message = "你需要至少一杯調茶才能完成長老交付的任務，請重新收集茶葉與配料後再行會見長老";

            GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.General);
            popup.transform.parent = GameObject.Find("UI").transform;
            popup.GetComponent<GPopup>().OpenPopup(title, message);
        }

        objectToDelete.Clear();
        UpdatePriceAndQuantity();
    }


    //UNITY EDITOR FUNCTION
    public void PrimaryPressed()
    {
        OnButtonPrimaryPressed?.Invoke();
        ClosePopup();
    }

    public void SecondaryPressed()
    {
        OnButtonSecondaryPressed?.Invoke();
        ClosePopup();
    }
}
