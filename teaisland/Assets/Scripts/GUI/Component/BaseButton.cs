using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class BaseButton : MonoBehaviour
{
    //SCRIPT用途：開發中。最底層的button物件。用於控制Button的行為。


    //DELEGATE


    //EVENT


    //屬性宣告
    public enum Sound
    {
        Primary,
        Secondary,
        Destructive
    }

    private AudioClip clip;

    private AudioClip primary, secondary, poko;


    //OnEnable, OnDisable


    //Awake, Start, Update
    private void Awake()
    {
        
    }


    //CORE FUNCTION
    public void PlayButtonSound(AudioSource source, Sound sound)
    {
        switch (sound)
        {
            case Sound.Primary:
                if (primary == null)
                {
                    primary = Resources.Load("Effect Sound/primary", typeof(AudioClip)) as AudioClip;
                }
                source.clip = primary;
                break;

            case Sound.Secondary:
                if (secondary == null)
                {
                    secondary = Resources.Load("Effect Sound/secondary", typeof(AudioClip)) as AudioClip;
                }
                source.clip = secondary;
                break;
            case Sound.Destructive:
                if (poko == null)
                {
                    poko = Resources.Load("Effect Sound/poko", typeof(AudioClip)) as AudioClip;
                }
                source.clip = poko;
                break;

            default:
                if (primary == null)
                {
                    primary = Resources.Load("Effect Sound/primary", typeof(AudioClip)) as AudioClip;
                }
                source.clip = primary;
                break;
        }
        
        source.Play();
    }


    //UNITY EDITOR FUNCTION
}
