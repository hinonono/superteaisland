using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialPopup : MonoBehaviour
{
    //SCRIPT用途：管理遊戲玩法教學的列表。


    //DELEGATE


    //EVENT


    //屬性宣告
    [SerializeField] private RectTransform container;
    [SerializeField] private CanvasGroup background;

    public List<GameObject> items;
    public Button btn_previous;
    public Button btn_next;

    private int currentIndex = 0;
    private float animateSpeed = 0.5f;


    //OnEnable, OnDisable


    //Awake, Start, Update


    //CORE FUNCTION
    public void OpenPopup()
    {
        //這裡的動畫是全部元件中最正確的版本
        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();


        //設定自身大小，並將自身的位置歸零
        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        gameObject.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;


        //設定背景大小，並將透明度設為0，並用動畫表示顯示過程
        background.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
        canvasGroup.alpha = 0;
        canvasGroup.LeanAlpha(1, animateSpeed);


        //將彈窗置於低「一個螢幕高」的位置，並用動畫表示歸零過程
        container.anchoredPosition = new Vector2(0, -Screen.height);
        container.LeanMoveY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;



        foreach (var item in items)
        {
            item.SetActive(false);
        }

        items[0].SetActive(true);
        btn_previous.interactable = false;
    }

    public void ClosePopup()
    {
        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();
        canvasGroup.LeanAlpha(0, animateSpeed);
        container.LeanMoveY(-Screen.height, animateSpeed).setEaseInExpo().setOnComplete(OnComplete);
    }


    void OnComplete()
    {
        gameObject.SetActive(false);
        DestroyPopup();
    }

    private void DestroyPopup()
    {
        Destroy(gameObject);
    }


    //UNITY EDITOR FUNCTION
    public void ForButtonNext()
    {
        if (currentIndex < items.Count - 1 )
        {
            items[currentIndex + 1].SetActive(true);
            items[currentIndex].SetActive(false);
            btn_previous.interactable = true;

            currentIndex++;
        } else
        {
            ClosePopup();
        }
    }

    public void ForButtonPrevious()
    {
        if (0 < currentIndex)
        {
            items[currentIndex - 1].SetActive(true);
            items[currentIndex].SetActive(false);

            currentIndex--;

            if (currentIndex == 0)
            {
                btn_previous.interactable = false;
            }
        } else
        {
            btn_previous.interactable = false;
        }
    }
}
