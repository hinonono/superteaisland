using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class InfoPopup : MonoBehaviour
{
    //SCRIPT用途：開發中。新的Info Popup，用來取代原本的Tea InfoPopup。


    //DELEGATE
    public delegate void ButtonPosDelegate(ButtonPos buttonPos);

    //EVENT
    public event ButtonPosDelegate OnAnyButtonPressed;
    public event Action OnButtonPrimaryPressed;
    public event Action OnButtonSecondaryPressed;


    //屬性宣告
    public enum ButtonPos
    {
        Primary,
        Secondary
    }

    [SerializeField] private float animateSpeed = 0.5f; //控制Pop出現與消失的速度
    [SerializeField] private Transform container;
    [SerializeField] private CanvasGroup background;
    [SerializeField] private TextMeshProUGUI popup_title;
    [SerializeField] private TextMeshProUGUI text_flavor;
    [SerializeField] private TextMeshProUGUI text_effect;
    [SerializeField] private Image popup_image;

    public GButton gButton_primary;
    public GButton gButton_secondary;


    //OnEnable, OnDisable


    //Awake, Start, Update
    private void Awake()
    {
        background.GetComponent<CanvasGroup>().alpha = 0;
        container.localPosition = new Vector2(0, -Screen.height);
        background.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
    }


    //CORE FUNCTION
    public void OpenPopup(ItemObject item)
    {
        string name = item.itemName;
        string flavor = item.flavor;
        string effect = item.effect;
        Sprite sprite = item.uiDisplay;

        popup_title.text = name;
        text_flavor.text = flavor;
        text_effect.text = effect;
        popup_image.sprite = sprite;

        gameObject.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();
        canvasGroup.LeanAlpha(1, animateSpeed);
        container.LeanMoveLocalY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;
    }

    public void ClosePopup()
    {
        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();
        canvasGroup.LeanAlpha(0, animateSpeed);
        container.LeanMoveLocalY(-Screen.height, animateSpeed).setEaseInExpo().setOnComplete(OnComplete);
    }

    void OnComplete()
    {
        gameObject.SetActive(false);
        Destroy(gameObject);
    }

    public void SetButtonText(ButtonPos pos, string text)
    {
        switch (pos)
        {
            case ButtonPos.Primary:
                gButton_primary.SetButtonText(text);

                break;
            case ButtonPos.Secondary:
                gButton_secondary.SetButtonText(text);

                break;
            default:
                break;
        }
    }


    //UNITY EDITOR FUNCTION
    public void PrimaryPressed()
    {
        OnAnyButtonPressed?.Invoke(ButtonPos.Primary);
        OnButtonPrimaryPressed?.Invoke();

        ClosePopup();
    }

    public void SecondaryPressed()
    {
        OnAnyButtonPressed?.Invoke(ButtonPos.Secondary);
        OnButtonSecondaryPressed?.Invoke();

        ClosePopup();
    }
}
