using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ServiceButton : BaseButton
{

    public enum ButtonType
    {
        Addition,
        subtraction,
        service,
        tableware,
        water,
        call
    }

    public Image image;             
    public ButtonType type;
    public TextMeshProUGUI text;

    private Button button;
    private AudioSource source;
    //OnEnable, OnDisable


    //Awake, Start, Update
    private void Awake()
    {
        button = gameObject.GetComponent<Button>();
        button.onClick.AddListener(this.OnButtonClicked);

        source = gameObject.GetComponent<AudioSource>();

        switch (type)
        {
            // case ButtonType.Addition:
            //     image.sprite = Resources.Load("GUI/Button/Service/Plus", typeof(Sprite)) as Sprite;

            //     break;
            // case ButtonType.subtraction:
            //     image.sprite = Resources.Load("GUI/Button/Service/Minus", typeof(Sprite)) as Sprite;

            //     break;
            case ButtonType.service:
                image.sprite = Resources.Load("GUI/Button/Service/service", typeof(Sprite)) as Sprite;
                text.color = new Color32(132, 79, 40, 255);
                break;
            case ButtonType.tableware:
                image.sprite = Resources.Load("GUI/Button/Service/service", typeof(Sprite)) as Sprite;
                text.color = new Color32(132, 79, 40, 255);
                break;
            case ButtonType.water:
                image.sprite = Resources.Load("GUI/Button/Service/service", typeof(Sprite)) as Sprite;
                text.color = new Color32(132, 79, 40, 255);
                break;
            case ButtonType.call:
                image.sprite = Resources.Load("GUI/Button/Service/service", typeof(Sprite)) as Sprite;
                text.color = new Color32(132, 79, 40, 255);
                break;
            default:
            
                break;
        }
    }

    private void OnDestroy()
    {
        button.onClick.RemoveListener(this.OnButtonClicked);
    }


    //CORE FUNCTION
    public void SetButtonText(string text)
    {
        this.text.text = text;
    }

    private void OnButtonClicked()
    {
        switch (type)
        {
            case ButtonType.Addition:
                PlayButtonSound(source, Sound.Primary);
                break;
            case ButtonType.subtraction:
                PlayButtonSound(source, Sound.Secondary);
                break;
            case ButtonType.service:
                PlayButtonSound(source, Sound.Primary);
                break;
            case ButtonType.tableware:
                PlayButtonSound(source, Sound.Primary);
                break;
            case ButtonType.water:
                PlayButtonSound(source, Sound.Primary);
                break;
            case ButtonType.call:
                PlayButtonSound(source, Sound.Primary);
                break;
            default:
                PlayButtonSound(source, Sound.Primary);
                break;
        }
    }

}
