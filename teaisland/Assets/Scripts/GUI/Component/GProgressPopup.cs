using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using static UnityEngine.Rendering.PostProcessing.SubpixelMorphologicalAntialiasing;

public class GProgressPopup : MonoBehaviour
{
    //SCRIPT用途：控制GProgressPopup的行為


    //DELEGATE


    //EVENT
    //public event Action OnButtonPrimaryPressed;
    //public event Action OnButtonSecondaryPressed;
    public event Action OnButtonCrossPressed;


    //屬性宣告
    [SerializeField] private RectTransform container;
    [SerializeField] private CanvasGroup background;
    [SerializeField] private GProgressPopupItemList itemList;

    private string currentSceneName;

    private float animateSpeed = 0.5f;


    //OnEnable, OnDisable


    //Awake, Start, Update


    //CORE FUNCTION
    public void OpenPopup()
    {
        //設定自身大小，並將自身的位置歸零
        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        gameObject.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

        //設定背景大小，並將透明度設為0，並用動畫表示顯示過程
        background.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();
        canvasGroup.alpha = 0;
        canvasGroup.LeanAlpha(1, animateSpeed);


        //將彈窗置於低「一個螢幕高」的位置，並用動畫表示歸零過程
        container.anchoredPosition = new Vector2(0, -Screen.height);
        container.LeanMoveY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;



        SetItemStatus();
    }

    private void DestroyPopup()
    {
        Destroy(gameObject);
    }

    public void ClosePopup()
    {
        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();
        canvasGroup.LeanAlpha(0, animateSpeed);
        container.LeanMoveLocalY(-Screen.height, animateSpeed).setEaseInExpo().setOnComplete(OnComplete);
    }

    void OnComplete()
    {
        gameObject.SetActive(false);
        DestroyPopup();
    }

    private void SetItemStatus()
    {
        currentSceneName = SceneSingleton.Instance.getCurrentSceneName();
        switch (currentSceneName)
        {
            case "Select_map":
                itemList.items[0].SetActive(true);
                for (int i = 0; i < 1; i++)
                {
                    itemList.items[i].SetStatus(GProgressPopupItem.Status.Unlocked);
                }
                break;
            case "Map_red":
                itemList.items[1].SetActive(true);
                for (int i = 0; i < 2; i++)
                {
                    itemList.items[i].SetStatus(GProgressPopupItem.Status.Unlocked);
                }
                break;
            case "Map_green":
                itemList.items[1].SetActive(true);
                for (int i = 0; i < 2; i++)
                {
                    itemList.items[i].SetStatus(GProgressPopupItem.Status.Unlocked);
                }
                break;
            case "Map_yellow":
                itemList.items[1].SetActive(true);
                for (int i = 0; i < 2; i++)
                {
                    itemList.items[i].SetStatus(GProgressPopupItem.Status.Unlocked);
                }
                break;
            case "Map_cyan":
                itemList.items[1].SetActive(true);
                for (int i = 0; i < 2; i++)
                {
                    itemList.items[i].SetStatus(GProgressPopupItem.Status.Unlocked);
                }
                break;
            case "Map_black":
                itemList.items[1].SetActive(true);
                for (int i = 0; i < 2; i++)
                {
                    itemList.items[i].SetStatus(GProgressPopupItem.Status.Unlocked);
                }
                break;
            case "Map_white":
                itemList.items[1].SetActive(true);
                for (int i = 0; i < 2; i++)
                {
                    itemList.items[i].SetStatus(GProgressPopupItem.Status.Unlocked);
                }
                break;
            case "Temple":
                itemList.items[2].SetActive(true);
                for (int i = 0; i < 3; i++)
                {
                    itemList.items[i].SetStatus(GProgressPopupItem.Status.Unlocked);
                }
                break;
            default:
                itemList.items[3].SetActive(true);
                for (int i = 0; i < 4; i++)
                {
                    itemList.items[i].SetStatus(GProgressPopupItem.Status.Unlocked);
                }
                break;
        }
    }


    //UNITY EDITOR FUNCTION
    public void CrossButtonPressed()
    {
        OnButtonCrossPressed?.Invoke();
        ClosePopup();
    }
}
