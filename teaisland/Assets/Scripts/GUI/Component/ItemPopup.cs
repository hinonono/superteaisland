using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ItemPopup : MonoBehaviour
{
    //SCRIPT用途：開發中。用於取代Tea ItemPopup。


    //DELEGATE
    public delegate void WithItem(ItemObject item);


    //EVENT
    public event Action OnConfirmPressed;
    public event Action OnCancelPressed;


    //屬性宣告
    [SerializeField] private float animateSpeed = 0.5f; //控制Pop出現與消失的速度
    [SerializeField] private Transform container;
    [SerializeField] private CanvasGroup background;
    [SerializeField] private TextMeshProUGUI popup_title;
    [SerializeField] private TextMeshProUGUI popup_description;
    [SerializeField] private Image popup_image;

    private ItemObject currentItem;


    //OnEnable, OnDisable


    //Awake, Start, Update
    private void Awake()
    {
        background.GetComponent<CanvasGroup>().alpha = 0;
        container.localPosition = new Vector2(0, -Screen.height);
        background.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
    }


    //CORE FUNCTION
    public void OpenPopup(ItemObject item)
    {
        currentItem = item;
        string name = item.itemName;
        string des = item.flavor;
        Sprite sprite = item.uiDisplay;

        popup_title.text = name;
        popup_image.sprite = sprite;

        gameObject.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();
        canvasGroup.LeanAlpha(1, animateSpeed);
        container.LeanMoveLocalY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;
    }

    public void ClosePopup()
    {
        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();
        canvasGroup.LeanAlpha(0, animateSpeed);
        container.LeanMoveLocalY(-Screen.height, animateSpeed).setEaseInExpo().setOnComplete(OnComplete);
    }

    void OnComplete()
    {
        gameObject.SetActive(false);
        Destroy(gameObject);
    }


    //UNITY EDITOR FUNCTION
    public void ConfirmPressed()
    {
        OnConfirmPressed?.Invoke();
        ClosePopup();
    }

    public void CancelPressed()
    {
        OnCancelPressed?.Invoke();
        ClosePopup();
    }
}
