using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CartPopupItem : MonoBehaviour
{
    //SCRIPT用途：控制購物車彈窗 -> 列表子項目 的行為


    //DELEGATE
    public delegate void WithGuid(Guid guid);

    //EVENT
    public event WithGuid OnDeleteButtonPressed;


    //屬性宣告
    [SerializeField] private TextMeshProUGUI text_title, text_tea, text_topping, text_quantity;
    public Guid id;



    //OnEnable, OnDisable


    //Awake, Start, Update


    //CORE FUNCTION
    public void SetCartPopupItem(CartItem cartItem)
    {
        text_title.text = cartItem.title;
        text_tea.text = cartItem.tea;
        text_topping.text = cartItem.topping;
        text_quantity.text = cartItem.GetQuantity();
        id = cartItem.id;
    }


    //UNITY EDITOR FUNCTION
    public void ForDeleteButton()
    {
        Debug.Log("Delete Pressed!");
        OnDeleteButtonPressed?.Invoke(id);
    }
}
