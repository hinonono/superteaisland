using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;
using Image = UnityEngine.UI.Image;


public class StaffPopup : MonoBehaviour
{
    // Start is called before the first frame update
    public delegate void ButtonPosDelegate(ButtonPos buttonPos);


    //EVENT
    public event ButtonPosDelegate OnAnyButtonPressed;
    public event Action OnButtonPrimaryPressed;

    //屬性宣告
    public enum ButtonPos
    {
        Primary,
    }

    [SerializeField] private RectTransform container;
    [SerializeField] private GameObject background;

    private float animateSpeed = 0.5f;

    public Button buttonPrimary;

    public TextMeshProUGUI titleGameObj;
    public TextMeshProUGUI messageGameObj;

    public GButton gButton_primary;

    public string title;
    public string message;


    //OnEnable, OnDisable
    private void OnEnable()
    {
        
    }


    //Awake, Start, Update
    private void Awake()
    {
        title = "Default Title";
        message = "Default Message";
        SetPopupText(title, message);

        gButton_primary.SetButtonText("確認");

        //background.GetComponent<CanvasGroup>().alpha = 0;
        //container.localPosition = new Vector2(0, -Screen.height);
        //background.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
    }

    //CORE FUNCTION
    public void SetPopupText(string title, string message)
    {
        titleGameObj.text = title;
        messageGameObj.text = message;
    }

    private void DestroyPopup()
    {
        Destroy(gameObject);
    }

    public void OpenPopup(string title, string message)
    {
        titleGameObj.text = title;
        messageGameObj.text = message;

        //gameObject.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
        //CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();
        //canvasGroup.LeanAlpha(1, animateSpeed);
        //container.LeanMoveLocalY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;


        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();


        //設定自身大小，並將自身的位置歸零
        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        gameObject.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;


        //設定背景大小，並將透明度設為0，並用動畫表示顯示過程
        background.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
        canvasGroup.alpha = 0;
        canvasGroup.LeanAlpha(1, animateSpeed);

        //將彈窗置於低「一個螢幕高」的位置，並用動畫表示歸零過程
        container.anchoredPosition = new Vector2(0, -Screen.height);
        container.LeanMoveY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;
    }

    public void ClosePopup()
    {
        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();
        canvasGroup.LeanAlpha(0, animateSpeed);
        container.LeanMoveLocalY(-Screen.height, animateSpeed).setEaseInExpo().setOnComplete(OnComplete);
    }

    void OnComplete()
    {
        gameObject.SetActive(false);
        DestroyPopup();
    }


    //public void SetButton(Sprite sprite, ButtonPos buttonPos)
    //{
    //    //DEPRECATED 不要使用！！
    //    switch (buttonPos)
    //    {
    //        case ButtonPos.Primary:
    //            buttonPrimary.gameObject.GetComponent<Image>().sprite = sprite;
    //            break;

    //        case ButtonPos.Secondary:
    //            buttonSecondary.gameObject.GetComponent<Image>().sprite = sprite;
    //            break;
    //        default:
    //            break;
    //    }
    //}

    public void SetButtonText(ButtonPos pos, string text)
    {
        switch (pos)
        {
            case ButtonPos.Primary:
                gButton_primary.SetButtonText(text);

                break;

            default:
                break;
        }
    }

    //UNITY EDITOR FUNCTION
    public void PrimaryPressed()
    {
        OnAnyButtonPressed?.Invoke(ButtonPos.Primary);
        OnButtonPrimaryPressed?.Invoke();

        ClosePopup();
    }

}
