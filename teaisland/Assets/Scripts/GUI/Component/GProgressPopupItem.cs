using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GProgressPopupItem : MonoBehaviour
{
    //SCRIPT用途：管理Popup裡Item的行為


    //DELEGATE


    //EVENT


    //屬性宣告
    public bool isActive = false;
    public GameObject icn_progress;
    public TextMeshProUGUI title;
    public Image background;

    public enum Status
    {
        Locked,
        Unlocked
    }


    //OnEnable, OnDisable


    //Awake, Start, Update


    //CORE FUNCTION
    public void SetStatus(Status status)
    {
        switch (status)
        {
            case Status.Locked:
                background.color = new Color(130, 130, 130, 255);
                title.color = GUIManager.Instance.color_primary;
                break;
            case Status.Unlocked:
                background.color = new Color(255, 255, 255, 255);
                //title.color = GUIManager.Instance.color_secondary;
                break;
            default:
                background.color = new Color(255, 255, 255, 255);
                //title.color = GUIManager.Instance.color_secondary;
                break;
        }
    }

    public void SetActive(bool isActive)
    {
        if (isActive)
        {
            icn_progress.SetActive(true);
        } else
        {
            icn_progress.SetActive(false);
        }
        this.isActive = isActive;
    }


    //UNITY EDITOR FUNCTION
}
