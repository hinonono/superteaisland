using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestructiveButton : BaseButton
{
    //SCRIPT用途：刪除型button（gbutton以外的button）。用於控制button的聲音等行為。


    //DELEGATE


    //EVENT


    //屬性宣告
    private Button button;
    private AudioSource source;


    //OnEnable, OnDisable


    //Awake, Start, Update
    private void Awake()
    {
        button = gameObject.GetComponent<Button>();
        button.onClick.AddListener(this.OnButtonClicked);
        source = gameObject.GetComponent<AudioSource>();
    }

    private void OnDestroy()
    {
        button.onClick.RemoveListener(this.OnButtonClicked);
    }


    //CORE FUNCTION
    private void OnButtonClicked()
    {
        PlayButtonSound(source, Sound.Destructive);
    }


    //UNITY EDITOR FUNCTION
}
