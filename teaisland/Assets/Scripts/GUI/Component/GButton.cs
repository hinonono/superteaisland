using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GButton : BaseButton
{
    //SCRIPT用途：開發中。用於Gpopup系列中的各種按鈕，按鈕文字可以動態產生。


    //DELEGATE


    //EVENT


    //屬性宣告
    public enum ButtonType
    {
        Primary,
        Secondary
    }

    public Image image;
    public ButtonType type;
    public TextMeshProUGUI text;

    private Button button;
    private AudioSource source;


    //OnEnable, OnDisable


    //Awake, Start, Update
    private void Awake()
    {
        button = gameObject.GetComponent<Button>();
        button.onClick.AddListener(this.OnButtonClicked);

        source = gameObject.GetComponent<AudioSource>();

        switch (type)
        {
            case ButtonType.Primary:
                image.sprite = Resources.Load("GUI/Button/primary_blank", typeof(Sprite)) as Sprite;

                break;
            case ButtonType.Secondary:
                image.sprite = Resources.Load("GUI/Button/secondary_blank", typeof(Sprite)) as Sprite;
                text.color = new Color32(132, 79, 40, 255);

                break;
            default:
                break;
        }
    }

    private void OnDestroy()
    {
        button.onClick.RemoveListener(this.OnButtonClicked);
    }


    //CORE FUNCTION
    public void SetButtonText(string text)
    {
        this.text.text = text;
    }

    private void OnButtonClicked()
    {
        switch (type)
        {
            case ButtonType.Primary:
                PlayButtonSound(source, Sound.Primary);
                break;
            case ButtonType.Secondary:
                PlayButtonSound(source, Sound.Secondary);
                break;
            default:
                PlayButtonSound(source, Sound.Primary);
                break;
        }
    }


    //UNITY EDITOR FUNCTION
}
