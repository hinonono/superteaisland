using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;
using Image = UnityEngine.UI.Image;

public class ServicePopup : MonoBehaviour
{
    // Start is called before the first frame update
    public delegate void ButtonPosDelegate(ButtonPos buttonPos);


    //EVENT
    public event ButtonPosDelegate OnAnyButtonPressed;

    public event Action OnButtonTablewarePressed;
    public event Action OnButtonWaterPressed;
    public event Action OnButtonCallPressed;
    public event Action OnButtonServicePressed;


    //屬性宣告
    public enum ButtonPos
    {
        service,
        tableware,
        water,
        call
    }

    [SerializeField] private RectTransform container;
    [SerializeField] private GameObject background;

    private float animateSpeed = 0.5f;

    public TextMeshProUGUI titleGameObj;
    public TextMeshProUGUI messageGameObj;
    public TextMeshProUGUI subtitleGameObj;

    public ServiceButton SButton_service;
    public ServiceButton SButton_tableware;
    public ServiceButton SButton_water;
    public ServiceButton SButton_call;
    // public GButton gButton_secondary;

    public string title;
    public string message;

    public string subtitle;


private void Awake()
    {
        title = "Default Title";
        message = "Default Message";
        SetPopupText(title, message);

        SButton_tableware.SetButtonText("請給我額外餐具");
        SButton_water.SetButtonText("請給我水");
        SButton_service.SetButtonText("東西打翻了");

        subtitle = "Default Message";
        SetPopupSubtitle(subtitle);
        SButton_call.SetButtonText("呼叫店員");

        //background.GetComponent<CanvasGroup>().alpha = 0;
        //container.localPosition = new Vector2(0, -Screen.height);
        //background.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
    }

    public void SetPopupText(string title, string message)
    {
        titleGameObj.text = title;
        messageGameObj.text = message;
    }

    public void SetPopupSubtitle(string subtitle)
    {
        subtitleGameObj.text = subtitle;
    }

    private void DestroyPopup()
    {
        Destroy(gameObject);
    }

    public void ServiceBell(string title, string message,string subtitle)
    {
        titleGameObj.text = title;
        messageGameObj.text = message;

        subtitleGameObj.text = subtitle;

        //gameObject.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
        //CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();
        //canvasGroup.LeanAlpha(1, animateSpeed);
        //container.LeanMoveLocalY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;


        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();


        //設定自身大小，並將自身的位置歸零
        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        gameObject.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;


        //設定背景大小，並將透明度設為0，並用動畫表示顯示過程
        background.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
        canvasGroup.alpha = 0;
        canvasGroup.LeanAlpha(1, animateSpeed);


        //將彈窗置於低「一個螢幕高」的位置，並用動畫表示歸零過程
        container.anchoredPosition = new Vector2(0, -Screen.height);
        container.LeanMoveY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;
    }

    public void ClosePopup()
    {
        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();
        canvasGroup.LeanAlpha(0, animateSpeed);
        container.LeanMoveLocalY(-Screen.height, animateSpeed).setEaseInExpo().setOnComplete(OnComplete);
    }

    void OnComplete()
    {
        gameObject.SetActive(false);
        DestroyPopup();
    }

    public void SetButtonText(ButtonPos pos, string text)
    {
        switch (pos)
        {
            case ButtonPos.service:
                SButton_service.SetButtonText(text);

                break;
            case ButtonPos.tableware:
                SButton_tableware.SetButtonText(text);

                break;
            case ButtonPos.water:
                SButton_water.SetButtonText(text);

                break;
            case ButtonPos.call:
                SButton_call.SetButtonText(text);

                break;
            default:
                break;
        }
    }

    public void TablewarePressed()
    {
        OnAnyButtonPressed?.Invoke(ButtonPos.tableware);
        OnButtonTablewarePressed?.Invoke();

        ClosePopup();
    }

    public void WaterPressed()
    {
        OnAnyButtonPressed?.Invoke(ButtonPos.water);
        OnButtonWaterPressed?.Invoke();

        ClosePopup();
    }

    public void CallPressed()
    {
        OnAnyButtonPressed?.Invoke(ButtonPos.call);
        OnButtonCallPressed?.Invoke();

        ClosePopup();
    }
    public void ServicePressed()
    {
        OnAnyButtonPressed?.Invoke(ButtonPos.service);
        OnButtonServicePressed?.Invoke();

        ClosePopup();
    }

    
    
}
