using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CartItem
{
    //SCRIPT用途：用於作為統一的物件Class傳入 購物車彈窗

    //CONSTRUCTOR
    public CartItem(string title, string tea, string topping, Guid guid)
    {
        this.id = guid;
        this.title = title;
        this.tea = tea;
        this.topping = topping;
        this.quantity = 1;
    }

    //屬性宣告
    public Guid id { get; }
    public string title;
    public string tea;
    public string topping;
    public int quantity;

    public Guid GetId()
    {
        return id;
    }

    public string GetQuantity()
    {
        return quantity.ToString();
    }

}
