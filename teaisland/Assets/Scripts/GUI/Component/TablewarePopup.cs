using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;
using Image = UnityEngine.UI.Image;

public class TablewarePopup : MonoBehaviour
{
    //SCRIPT用途：開發中。最一般的Popup。


    //DELEGATE
    public delegate void ButtonPosDelegate(ButtonPos buttonPos);


    //EVENT
    public event ButtonPosDelegate OnAnyButtonPressed;
    public event Action OnButtonPrimaryPressed;
    public event Action OnButtonSecondaryPressed;
    public event Action OnButtonAdditionPressed;
    public event Action OnButtonsubtractionPressed;

    //屬性宣告
    public enum ButtonPos
    {
        Primary,
        Secondary,
        Addition,
        Addition2,
        Addition3,
        subtraction,
        subtraction2,
        subtraction3
    }

    [SerializeField] private RectTransform container;
    [SerializeField] private GameObject background;

      public TextMeshProUGUI nameGameObj;
    public TextMeshProUGUI name2GameObj;
    public TextMeshProUGUI name3GameObj;

    private float animateSpeed = 0.5f;

    public Button buttonPrimary;
    public Button buttonSecondary;
    public Button subtraction;
    public Button subtraction2;
    public Button subtraction3;
     public ServiceButton SButton_Addition;
     public ServiceButton SButton_Addition2;
     public ServiceButton SButton_Addition3;
    public ServiceButton SButton_subtraction;
    public ServiceButton SButton_subtraction2;
    public ServiceButton SButton_subtraction3;

    public string Name;
    public string Name2;
    public string Name3;

    public static int Score;
    public TextMeshProUGUI textshow; 
    public TextMeshProUGUI textshow2; 
    public TextMeshProUGUI textshow3; 


    public TextMeshProUGUI titleGameObj;
    public TextMeshProUGUI messageGameObj;

    public GButton gButton_primary;
    public GButton gButton_secondary;

    public string title;
    public string message;

      int a = 0;
      int b = 0;
      int c = 0;

    //OnEnable, OnDisable
    private void OnEnable()
    {
        
    }


    //Awake, Start, Update
    private void Awake()
    {
        title = "Default Title";
        message = "Default Message";
        SetPopupText(title, message);

        gButton_primary.SetButtonText("返回");
        gButton_secondary.SetButtonText("取消");

        Name = "Default Title";
        Name2 = "Default Title";
        Name3 = "Default Title";
        SetItemText(Name, Name2, Name3);

        //background.GetComponent<CanvasGroup>().alpha = 0;
        //container.localPosition = new Vector2(0, -Screen.height);
        //background.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
    }

    public void SetItemText(string Name, string Name2, string Name3)
    {
        nameGameObj.text = Name;
        name2GameObj.text = Name2;
        name3GameObj.text = Name3;
    }

    //CORE FUNCTION
    public void SetPopupText(string title, string message)
    {
        titleGameObj.text = title;
        messageGameObj.text = message;
    }

    private void DestroyPopup()
    {
        Destroy(gameObject);
    }

    public void OpenPopup(string title, string message, string Name, string Name2, string Name3)
    {
        titleGameObj.text = title;
        messageGameObj.text = message;

        nameGameObj.text = Name;
        name2GameObj.text = Name2;
        name3GameObj.text = Name3;

        //gameObject.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
        //CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();
        //canvasGroup.LeanAlpha(1, animateSpeed);
        //container.LeanMoveLocalY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;


        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();


        //設定自身大小，並將自身的位置歸零
        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        gameObject.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;


        //設定背景大小，並將透明度設為0，並用動畫表示顯示過程
        background.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
        canvasGroup.alpha = 0;
        canvasGroup.LeanAlpha(1, animateSpeed);

        //將彈窗置於低「一個螢幕高」的位置，並用動畫表示歸零過程
        container.anchoredPosition = new Vector2(0, -Screen.height);
        container.LeanMoveY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;
    }

    public void ClosePopup()
    {
        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();
        canvasGroup.LeanAlpha(0, animateSpeed);
        container.LeanMoveLocalY(-Screen.height, animateSpeed).setEaseInExpo().setOnComplete(OnComplete);
    }

    void OnComplete()
    {
        gameObject.SetActive(false);
        DestroyPopup();
    }

    public void PlusNumber(){
        a = a + 1;    
        textshow.text = "" + a; 
        ButtonAble();
      Debug.Log ( textshow.text );
    }
    public void PlusNumber2(){
        b = b + 1;    
        textshow2.text = "" + b; 
        ButtonAble2();
      Debug.Log ( textshow2.text );

    }
    public void PlusNumber3(){
        c = c + 1;    
        textshow3.text = "" + c; 
        ButtonAble3();
      Debug.Log ( textshow3.text );
      
    }
    public void MinusNumber(){
      // a = a - 1;    
      // textshow.text = "" + a; 
      // Debug.Log ( textshow.text );

      if (a < 1 ) {
            DisableButton ();
            Debug.Log ( "數量不得小於0" );
      }
      else if  (a >= 0){
            // ButtonAble();
            a = a - 1;    
            textshow.text = "" + a; 
            Debug.Log ( textshow.text );
      }
      
    }
    public void MinusNumber2(){
      

      if (b < 1 ) {
            DisableButton2 ();
            Debug.Log ( "數量不得小於0"  );
      }
      else if  (b >= 1){
            b = b - 1;    
            textshow2.text = "" + b; 
            Debug.Log ( textshow2.text );
      }
      }
      public void MinusNumber3(){
      
      if (c < 1 ) {
            DisableButton3 ();
            Debug.Log ( "數量不得小於0" );
      }
      else if  (c >= 1){
            c = c - 1;    
            textshow3.text = "" + c; 
            Debug.Log ( textshow3.text );

      }}
      

      


      //public void SetButton(Sprite sprite, ButtonPos buttonPos)
      //{
      //    //DEPRECATED 不要使用！！
      //    switch (buttonPos)
      //    {
      //        case ButtonPos.Primary:
      //            buttonPrimary.gameObject.GetComponent<Image>().sprite = sprite;
      //            break;

      //        case ButtonPos.Secondary:
      //            buttonSecondary.gameObject.GetComponent<Image>().sprite = sprite;
      //            break;
      //        default:
      //            break;
      //    }
      //}

      public void SetButtonText(ButtonPos pos, string text)
    {
        switch (pos)
        {
            case ButtonPos.Primary:
                gButton_primary.SetButtonText(text);

                break;
            case ButtonPos.Secondary:
                gButton_secondary.SetButtonText(text);

                break;
            default:
                break;
        }
    }

    //UNITY EDITOR FUNCTION
    public void PrimaryPressed()
    {
        OnAnyButtonPressed?.Invoke(ButtonPos.Primary);
        OnButtonPrimaryPressed?.Invoke();

        ClosePopup();
    }

    public void SecondaryPressed()
    {
        OnAnyButtonPressed?.Invoke(ButtonPos.Secondary);
        OnButtonSecondaryPressed?.Invoke();

        ClosePopup();
    }

    public void AdditionPressed()
    {
        OnAnyButtonPressed?.Invoke(ButtonPos.Addition);
        OnButtonAdditionPressed?.Invoke();

        PlusNumber();
    }
    public void Addition2Pressed()
    {
        OnAnyButtonPressed?.Invoke(ButtonPos.Addition2);
        OnButtonAdditionPressed?.Invoke();

        PlusNumber2();
    }
    public void Addition3Pressed()
    {
        OnAnyButtonPressed?.Invoke(ButtonPos.Addition3);
        OnButtonAdditionPressed?.Invoke();

        PlusNumber3();
    }

    public void SubtractionPressed()
    {
        OnAnyButtonPressed?.Invoke(ButtonPos.subtraction);
        OnButtonsubtractionPressed?.Invoke();

        MinusNumber();
    }
    public void Subtraction2Pressed()
    {
        OnAnyButtonPressed?.Invoke(ButtonPos.subtraction2);
        OnButtonsubtractionPressed?.Invoke();

        MinusNumber2();
    }
    public void Subtraction3Pressed()
    {
        OnAnyButtonPressed?.Invoke(ButtonPos.subtraction3);
        OnButtonsubtractionPressed?.Invoke();

        MinusNumber3();
    }
      private void DisableButton()
      {
            subtraction.GetComponent<Button> ().interactable = false;
      }
      private void DisableButton2()
      {
           subtraction2.GetComponent<Button> ().interactable = false;
      }
      private void DisableButton3()
      {
            subtraction3.GetComponent<Button> ().interactable = false;
      }
      private void ButtonAble()
      {
            subtraction.interactable = true;
      }
      private void ButtonAble2()
      {
           subtraction2.interactable = true;
      }
      private void ButtonAble3()
      {
            subtraction3.interactable = true;
      }
}