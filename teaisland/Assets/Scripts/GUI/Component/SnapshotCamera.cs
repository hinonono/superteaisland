using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class SnapshotCamera : MonoBehaviour
{
    //SCRIPT用途：開發中，用於擷取螢幕畫面。


    //DELEGATE


    //EVENT


    //屬性宣告
    private Camera snapCam;

    private int resWidth = 256;
    private int resHeight = 256;

    private Player inputs;


    //OnEnable, OnDisable
    private void OnEnable()
    {
        inputs = new Player();
        inputs.Util.Enable();

        inputs.Util.Snapshot.performed += Snapshot_performed;
    }

    private void Snapshot_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        CallTakeSnapshot();
    }

    private void OnDisable()
    {
        
    }


    //Awake, Start, Update
    private void Awake()
    {
        snapCam = GetComponent<Camera>();
        if (snapCam.targetTexture == null)
        {
            snapCam.targetTexture = new RenderTexture(resWidth, resHeight, 24);
        }
        else
        {
            resWidth = snapCam.targetTexture.width;
            resHeight = snapCam.targetTexture.height;
        }
        snapCam.gameObject.SetActive(false);
    }

    private void LateUpdate()
    {
        if (snapCam.gameObject.activeInHierarchy)
        {
            Texture2D snapshot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
            snapCam.Render();
            RenderTexture.active = snapCam.targetTexture;
            snapshot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);

            byte[] bytes = snapshot.EncodeToPNG();
            string fileName = SnapshotName();
            System.IO.File.WriteAllBytes(fileName, bytes);

            Debug.Log("Taken");
            snapCam.gameObject.SetActive(false);
        }

        inputs.Util.Disable();
    }


    //CORE FUNCTION
    public void CallTakeSnapshot()
    {
        snapCam.gameObject.SetActive(true);
    }

    public string SnapshotName()
    {
        return string.Format("{0}/Snapshots/snap_{1}x{2}_{3}.png",
            Application.dataPath,
            resWidth,
            resHeight,
            System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
    }


    //UNITY EDITOR FUNCTION
}
