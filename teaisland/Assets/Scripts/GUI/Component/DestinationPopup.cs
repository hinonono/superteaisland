using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DestinationPopup : MonoBehaviour
{
    //SCRIPT用途：開發中。新用於控制選擇地圖場景專用的彈窗。


    //DELEGATE


    //EVENT
    public event Action<int> OnButtonPrimaryPressed;
    public event Action OnButtonSecondaryPressed;


    //屬性宣告
    [SerializeField] private MenuReader menuReader;
    [SerializeField] private Text able_list, tea_flavor, tea_effect;
    [SerializeField] private TextMeshProUGUI area_name, text_tea, topping_1, topping_2;

    [SerializeField] private RectTransform container;
    [SerializeField] private CanvasGroup background;

    [SerializeField] private Image img_scene, img_tea, img_topping_1, img_topping_2;
    public Sprite[] sceneSnapshot;

    [SerializeField] private GButton btn_confirm, btn_cancel;

    private float animateSpeed = 0.5f;
    private int id;


    //OnEnable, OnDisable


    //Awake, Start, Update
    private void Awake()
    {
        btn_confirm.SetButtonText("確認");
        btn_cancel.SetButtonText("取消");
    }


    //CORE FUNCTION
    public void OpenPopup(int id)
    {
        this.id = id;
        InitPopupText(this.id);

        //這裡的動畫是全部元件中最正確的版本
        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();


        //設定自身大小，並將自身的位置歸零
        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        gameObject.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;


        //設定背景大小，並將透明度設為0，並用動畫表示顯示過程
        background.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
        canvasGroup.alpha = 0;
        canvasGroup.LeanAlpha(1, animateSpeed);


        //將彈窗置於低「一個螢幕高」的位置，並用動畫表示歸零過程
        container.anchoredPosition = new Vector2(0, -Screen.height);
        container.LeanMoveY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;
    }

    public void ClosePopup()
    {
        CanvasGroup canvasGroup = background.GetComponent<CanvasGroup>();
        canvasGroup.LeanAlpha(0, animateSpeed);
        container.LeanMoveY(-Screen.height, animateSpeed).setEaseInExpo().setOnComplete(OnComplete);
    }


    void OnComplete()
    {
        gameObject.SetActive(false);
        DestroyPopup();
    }

    private void DestroyPopup()
    {
        Destroy(gameObject);
    }

    public void InitPopupText(int id)
    {
        //VOID用途：依據不同的場景設定POPUP中的文字
        var t = menuReader.myuiteaList;

        string pure_str = "";
        for (var i = 0; i < t.uitea[id].uinfo.coordination.Length; i++)
        {
            pure_str += t.uitea[id].uinfo.coordination[i].ToString();
            if (i < t.uitea[id].uinfo.coordination.Length - 1)
            {
                pure_str += ", ";
            }

        }

        img_topping_1.sprite = InitPopupImage("Item/topping/item_topping_" + t.uitea[id].uitopping[0].imagepath);
        img_topping_2.sprite = InitPopupImage("Item/topping/item_topping_" + t.uitea[id].uitopping[1].imagepath);
        img_tea.sprite = InitPopupImage("Item/tea/item_tea_" + t.uitea[id].imagepath);


        area_name.text = t.uitea[id].areaname;
        able_list.text = pure_str;
        tea_flavor.text = t.uitea[id].uinfo.flavor;
        tea_effect.text = t.uitea[id].uinfo.effect;
        topping_1.text = t.uitea[id].uitopping[0].name;
        topping_2.text = t.uitea[id].uitopping[1].name;
        text_tea.text = t.uitea[id].name;
        img_scene.sprite = sceneSnapshot[id];
    }

    public Sprite InitPopupImage(string path)
    {
        Sprite s = Resources.Load<Sprite>(path);
        return s;
    }

    //UNITY EDITOR FUNCTION
    public void PrimaryPressed()
    {
        OnButtonPrimaryPressed?.Invoke(this.id);
        ClosePopup();
    }

    public void SecondaryPressed()
    {
        OnButtonSecondaryPressed?.Invoke();
        ClosePopup();
    }
}
