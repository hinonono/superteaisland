using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectMap_Animation : MonoBehaviour
{
    [SerializeField]
    private GameObject[] buttons;

    [SerializeField]
    private float delay;

    [SerializeField]
    private float duration;

    private int index = 0;
    private float current = 0;

    private void Start()
    {
        foreach (var b in buttons)
        {
            LeanTween.alpha(b.GetComponent<Button>().image.rectTransform, 0, 0);
        }

        StartCoroutine(Wait(index, current));
    }

    IEnumerator Wait(int i, float c)
    {
        yield return new WaitForSeconds(c);

        Vector3 pos = new Vector3(buttons[i].transform.position.x, buttons[i].transform.position.y + 100, buttons[i].transform.position.z);
        LeanTween.move(buttons[i], pos, duration).setEase(LeanTweenType.easeInSine);

        LeanTween.alpha(buttons[i].GetComponent<Button>().image.rectTransform, 1, duration).setEase(LeanTweenType.easeInSine);

        if (index < buttons.Length)
        {
            index++;
        }

        if (current == 0)
        {
            current += delay;
        }

        if (index <= buttons.Length - 1)
        {
            StartCoroutine(Wait(index, current));
        }
    }
}
