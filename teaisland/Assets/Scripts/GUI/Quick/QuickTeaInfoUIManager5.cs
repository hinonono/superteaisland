using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class QuickTeaInfoUIManager5 : MonoBehaviour
{


    public float animateSpeed = 0.5f;
    public GameObject popup;

    private string sceneDestination;

    // public Transform box;
    // public CanvasGroup background;

    //public Text popup_areaName;
    //public Text popup_teaName;

    //嘗試簡化宣告
    public GameObject popup_content;
    // public GameObject topping_content;
    private string pure_str;
    [SerializeField] private Image img_info;

    //那個JSON的Script
    private MenuReader menuReader;


    private void Start()
    {
        menuReader = gameObject.GetComponent<MenuReader>();
    }

    public void SetSceneDestination(string destination)
    {
        popup.SetActive(true);
        sceneDestination = destination;
        

        switch (destination)
        {
            case "1":
                setPopupText(0);
                break;

            case "2":
                setPopupText(1);
                break;
        }

        showPopup();

    }

    private void setPopupText(int id)
    {
        //VOID用途：依據不同的場景設定POPUP中的文字

        //把該宣告的子項目宣告出來
        TextMeshProUGUI info_flavor = popup_content.transform.Find("Text_Flavor").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI info_effect = popup_content.transform.Find("Text_Effect").GetComponent<TextMeshProUGUI>();

        TextMeshProUGUI text_title = popup_content.transform.Find("Text_title").GetComponent<TextMeshProUGUI>();
      

        img_info.sprite = InitPopupImage("Item/infoImg/item_topping_" + menuReader.myuiteaList.uitea[4].uitopping[id].imagepath);


        text_title.text = menuReader.myuiteaList.uitea[4].uitopping[id].name; 
        info_flavor.text = menuReader.myuiteaList.uitea[4].uitopping[id].uinfo.flavor;
      //   able_list.text = menuReader.myuiteaList.uitea[id].;
        info_effect.text = menuReader.myuiteaList.uitea[4].uitopping[id].uinfo.effect;

        Debug.Log(menuReader.myuiteaList.uitea[4].areaname);

    }
    public Sprite InitPopupImage(string path)
    {
        Sprite s = Resources.Load<Sprite>(path);
        return s;
    }

    public void goToScene(string destination)
    {
        if (destination != null)
        {
            SceneManager.LoadScene(destination);
        }
        else
        {
            Debug.Log("Scene Destination is not set yet. Please fill in scene name first.");
        }
    }

        private void showPopup()
    {
        //VOID用途：將Popup打開

        // background.alpha = 0;
        // background.LeanAlpha(0, animateSpeed);

        // box.localPosition = new Vector2(0, 0);
        // box.LeanMoveLocalY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;
    }

        public void ClosePopup()
    {
        //VOID用途：將Popup關起來

        // background.LeanAlpha(0, animateSpeed);
        // box.LeanMoveLocalY(-Screen.height, animateSpeed).setEaseInExpo().setOnComplete(onComplete);
    }

    void onComplete()
    {
        pure_str = "";
        popup.SetActive(false);
    }
}

