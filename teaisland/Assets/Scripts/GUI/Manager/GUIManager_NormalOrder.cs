using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIManager_NormalOrder : MonoBehaviour
{
    //SCRIPT用途：


    //DELEGATE


    //EVENT


    //屬性宣告
    [SerializeField] public PlayerInventory playerInventory;


    //OnEnable, OnDisable


    //Awake, Start, Update


    //CORE FUNCTION


    //UNITY EDITOR FUNCTION
    public void ForCartButton()
    {
        //用途：按下購物車按鈕時生成購物車彈窗

        GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.Cart);
        popup.transform.parent = gameObject.transform;
        popup.GetComponent<CartPopup>().OpenPopup();
        popup.GetComponent<CartPopup>().InstantiateCartItem(playerInventory.inventory);

    }
}
