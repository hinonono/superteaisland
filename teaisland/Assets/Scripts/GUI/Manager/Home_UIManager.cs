using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Home_UIManager : MonoBehaviour
{
    private Player inputs;

    private void Awake()
    {
        inputs = new Player();
        inputs.Touch.Enable();
    }

    private void Start()
    {
        StartCoroutine(GoToOpeningVideoCo());
    }

    private void Update()
    {
        if (inputs.Touch.PrimaryContact.triggered)
        {
            StopAllCoroutines();
            StartCoroutine(GoToOpeningVideoCo());
        }
    }

    IEnumerator GoToOpeningVideoCo()
    {
        yield return new WaitForSecondsRealtime(300);
        GoToOpeningVideo();
    }

    public void StartGameOrder()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.Select_map.ToString());
    }
    public void AboutTaiocha()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.AboutTaiocha.ToString());
    }
    public void BackToHome()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.Home.ToString());
    }

    public void QuickOrder()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.Quick_tea.ToString());
    }

    public void GoToOpeningVideo()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.OpeningVideo.ToString());
    }

    public void GoToQrForExhibit()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.QrForExhibit.ToString());
    }
}
