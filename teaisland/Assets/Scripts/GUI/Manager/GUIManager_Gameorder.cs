using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;
using static System.Net.Mime.MediaTypeNames;

public class GUIManager_Gameorder : MonoBehaviour
{
    //SCRIPT用途：開發中。附加於GUI CANVAS之上，管理所有的UI元件。


    //DELEGATE


    //EVENT


    //屬性宣告
    [SerializeField] private SelectTarget selectTarget;
    [SerializeField] public PlayerInventory playerInventory;
    [SerializeField] private DisplayInventory displayInventory;

    [SerializeField] private Button nextButton;
    [SerializeField] private Button cartButton, progressButton, infoButton;
    [SerializeField] private bool findHintArea;
    [SerializeField] private GameObject GameOperationUI;
    [SerializeField] private GameObject InventoryCanvas;

    public bool showGameOperationUI = true;
    public bool showInventoryCanvas = true;

    private ItemObject currentItem;
    private GameObject[] walls;
    private GameObject[] hintAreas;


    //OnEnable, OnDisable
    private void OnEnable()
    {
        selectTarget.onItemTouched += SelectTarget_onItemTouched;
        selectTarget.onItemHold += SelectTarget_onItemHold;
        selectTarget.onItemIntendToInteract += SelectTarget_onItemIntendToInteract;
        selectTarget.onPersonTouched += SelectTarget_onPersonTouched;

        playerInventory.onCollectCompleted += PlayerInventory_onCollectCompleted;
        playerInventory.onCollectNotCompleted += PlayerInventory_onCollectNotCompleted;
    }

    private void OnDisable()
    {
        selectTarget.onItemTouched -= SelectTarget_onItemTouched;
        selectTarget.onItemHold -= SelectTarget_onItemHold;
        selectTarget.onItemIntendToInteract -= SelectTarget_onItemIntendToInteract;
        selectTarget.onPersonTouched -= SelectTarget_onPersonTouched;

        playerInventory.onCollectCompleted -= PlayerInventory_onCollectCompleted;
        playerInventory.onCollectNotCompleted -= PlayerInventory_onCollectNotCompleted;
    }

    private void OnDestroy()
    {
        //if (findHintArea)
        //{
        //    for (int i = 0; i < hintAreas.Length; i++)
        //    {
        //        //hintAreas[i].GetComponent<HintArea>().onHintAreaTouched -= HintArea_onHintAreaTouched;
        //    }
        //}

    }


    //Awake, Start, Update
    private void Awake()
    {
        displayInventory.UpdateDisplay();
        nextButton.interactable = false;

        SubscribeToWall();
        SubscribeToHintArea();

    }

    private void Start()
    {
        ShowAlertWhenSceneLoaded();

        GameOperationUI.SetActive(showGameOperationUI);
        InventoryCanvas.SetActive(showInventoryCanvas);

        if (SceneSingleton.Instance.getCurrentSceneName() == SceneSingleton.SceneList.Temple.ToString())
        {
            ShowInventoryCanvas(false);

        }
    }


    //CORE FUNCTION

    public void ShowGameOperationUI(bool status)
    {
        GameOperationUI.SetActive(status);
        showGameOperationUI = GameOperationUI.activeSelf;
    }

    public void ShowInventoryCanvas(bool status)
    {
        InventoryCanvas.SetActive(status);
        showInventoryCanvas = InventoryCanvas.activeSelf;

    }

    private void ShowAlertWhenSceneLoaded()
    {

        if (SceneSingleton.Instance.getCurrentSceneName() == SceneSingleton.SceneList.Temple.ToString())
        {
            GameObject alert = GUIManager.Instance.InstantiateAlert();
            alert.transform.parent = gameObject.transform;
            alert.GetComponent<GAlert>().ShowAlert("請前往祭壇上方去拜見長老", 5);

        }
        else if (SceneSingleton.Instance.getCurrentSceneName() == SceneSingleton.SceneList.NameTeaCard.ToString())
        {
            GameObject alert = GUIManager.Instance.InstantiateAlert();
            alert.transform.parent = gameObject.transform;
            alert.GetComponent<GAlert>().ShowAlert("請在卡片上替這杯茶命名", 10);
        }
        else
        {
            GameObject alert = GUIManager.Instance.InstantiateAlert();
            alert.transform.parent = gameObject.transform;
            alert.GetComponent<GAlert>().ShowAlert("收集至少一種茶葉與配料去拜見長老吧！", 10);
        }
    }

    private void SubscribeToHintArea()
    {
        //VOID用途：訂閱Hint Area

        if (findHintArea)
        {
            Debug.Log("Tea Alert will search for HintArea in the scene now.");
            hintAreas = GameObject.FindGameObjectsWithTag("HintArea");
            if (hintAreas == null)
            {
                Debug.Log("hintAreas array is empty");
            }
            else
            {
                for (int i = 0; i < hintAreas.Length; i++)
                {
                    hintAreas[i].GetComponent<HintArea>().onHintAreaTouched += HintArea_onHintAreaTouched;
                }
            }
        }
    }

    private void SubscribeToWall()
    {
        //VOID用途：訂閱牆壁的事件

        walls = GameObject.FindGameObjectsWithTag("Wall");
        for (int i = 0; i < walls.Length; i++)
        {
            walls[i].GetComponent<WallDetecter>().onWallTouched += WallDetecter_onWallTouched;
        }
    }

    private void WallDetecter_onWallTouched(object sender, EventArgs e)
    {
        string[] texts =
        {
            "似乎有股神秘的力量阻擋我們不能夠再往前進...",
            "前方似乎沒有路了，換個方向前進試試看吧！"
        };

        string text = texts[Random.Range(0, texts.Length)];

        GameObject alert = GUIManager.Instance.InstantiateAlert();
        alert.transform.parent = gameObject.transform;
        alert.GetComponent<GAlert>().ShowAlert(text);
    }

    private void HintArea_onHintAreaTouched(string text)
    {
        GameObject alert = GUIManager.Instance.InstantiateAlert();
        alert.transform.parent = gameObject.transform;
        alert.GetComponent<GAlert>().ShowAlert(text);

        //InstantiateAlert(text);
    }

    private void SelectTarget_onItemTouched(GroundItem item)
    {
        //currentItem = item.item;
        //GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.Info);
        //popup.transform.parent = gameObject.transform;
        //popup.GetComponent<InfoPopup>().OpenPopup(currentItem);

        //InstantiatePopup(PopupTypeList.Info);
    }

    private void SelectTarget_onItemHold(GroundItem item)
    {
        currentItem = item.item;

        GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.Info);
        popup.transform.parent = gameObject.transform;

        popup.GetComponent<InfoPopup>().SetButtonText(InfoPopup.ButtonPos.Primary, "採集材料");
        popup.GetComponent<InfoPopup>().SetButtonText(InfoPopup.ButtonPos.Secondary, "取消");
        popup.GetComponent<InfoPopup>().OpenPopup(currentItem);

        popup.GetComponent<InfoPopup>().OnButtonPrimaryPressed += DoPickupItem;


        //舊版OnItemHold程式碼
        //currentItem = item.item;
        //GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.Pick);
        //popup.transform.parent = gameObject.transform;
        //popup.GetComponent<ItemPopup>().OpenPopup(currentItem);
        //popup.GetComponent<ItemPopup>().OnConfirmPressed += DoPickupItem;

        //InstantiatePopup(PopupTypeList.Pick);
    }

    private void SelectTarget_onItemIntendToInteract()
    {
        string message = "在材料上長壓，將其採集起來";

        GameObject alert = GUIManager.Instance.InstantiateAlert();
        alert.transform.parent = gameObject.transform;
        alert.GetComponent<GAlert>().ShowAlert(message);

        //InstantiateAlert(message);
    }

    private void SelectTarget_onPersonTouched(GameObject recievedGameObj)
    {
        //開始對話
        recievedGameObj.GetComponent<DialogueTrigger>().StartDialogue();
        DialogueManager.Instance.OnDialogueEnded += Instance_OnDialogueEnded;
    }

    private void Instance_OnDialogueEnded()
    {
        //呼叫彈窗
        string title = "本輪遊戲已結束";
        string content = "遊戲體驗至此已全部結束。若需採集第二杯茶品，請將平板交給下一位玩家並選擇「繼續點餐」";

        GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.General);
        popup.transform.parent = gameObject.transform;
        popup.GetComponent<GPopup>().SetButtonText(GPopup.ButtonPos.Primary, "結賬");
        popup.GetComponent<GPopup>().SetButtonText(GPopup.ButtonPos.Secondary, "繼續點餐");
        popup.GetComponent<GPopup>().OpenPopup(title, content);
        popup.GetComponent<GPopup>().OnButtonPrimaryPressed += GoToPay;
        popup.GetComponent<GPopup>().OnButtonSecondaryPressed += OrderAgain;
    }

    private void GoToPay()
    {
        SceneManager.LoadScene("Quick_pay");
    }

    private void OrderAgain()
    {
        GoToScene_SelectMap();
    }

    private void PlayerInventory_onCollectCompleted()
    {
        nextButton.interactable = true;
        ForButtonNext();
    }

    private void PlayerInventory_onCollectNotCompleted()
    {

    }

    private void DoPickupItem()
    {
        //判斷材料庫是否已有配料

        if (playerInventory.GetHasTopping() == true && currentItem.type == ItemType.Topping)
        {
            //目前有配料，然後他想採第二種配料時。
            string title = "無法加入材料庫";
            string content = "你的材料庫已經有" + playerInventory.GetCurrentToppingName() + "了！材料庫裡同時最多只能放下一種配料，如果按下繼續將會覆蓋之前採集的配料！";

            GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.General);
            popup.transform.parent = gameObject.transform;
            popup.GetComponent<GPopup>().SetButtonText(GPopup.ButtonPos.Primary, text: "覆蓋");
            popup.GetComponent<GPopup>().SetButtonText(GPopup.ButtonPos.Secondary, text: "取消");
            popup.GetComponent<GPopup>().OpenPopup(title, content);
            popup.GetComponent<GPopup>().OnButtonPrimaryPressed += DoReplaceTopping;

        } else
        {
            playerInventory.AddItem(currentItem);
            string message = "一份 " + currentItem.itemName + " 已放入你的材料庫中！";

            GameObject alert = GUIManager.Instance.InstantiateAlert();
            alert.transform.parent = gameObject.transform;
            alert.GetComponent<GAlert>().ShowAlert(message);

            //InstantiateAlert(message);
        }
    }

    private void DoReplaceTopping()
    {
        playerInventory.DeleteItemBy(playerInventory.GetCurrentToppingId());
        playerInventory.SetHasTopping(false);
        DoPickupItem();

        Debug.Log("count : " + playerInventory.inventory.Container.Items.Count);
        displayInventory.UpdateDisplay(mode: DisplayInventory.UpdateMode.Forced);
    }

    private void GoToScene_SelectMap()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.Select_map.ToString());
    }

    private void GoToScene_NameTeaCard()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.NameTeaCard.ToString());
    }

    private void GoToScene_Home()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.Home.ToString());
    }

    //UNITY EDITOR FUNCTION
    public void GoToScence(string des)
    {
        SceneManager.LoadScene(des);
    }

    public void ForButtonBack()
    {
        GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.General);
        popup.transform.parent = gameObject.transform;
        string title = "";
        string message = "";

        if (SceneSingleton.Instance.getCurrentSceneName() == "Select_map")
        {
            if (playerInventory.inventory.TiaochaContainer.Items.Count != 0)
            {
                title = "返回主選單";
                message = "您的購物車裡有尚未結帳的商品。如果離開，購物車將會被重置！";
                popup.GetComponent<GPopup>().SetButtonText(GPopup.ButtonPos.Primary, "我再想想");
                popup.GetComponent<GPopup>().SetButtonText(GPopup.ButtonPos.Secondary, "離開");
                popup.GetComponent<GPopup>().OnButtonSecondaryPressed += GoToScene_Home;
            }
            else
            {
                title = "取消遊戲";
                message = "確認要結束遊戲模式？";

                popup.GetComponent<GPopup>().SetButtonText(GPopup.ButtonPos.Primary, "我再想想");
                popup.GetComponent<GPopup>().SetButtonText(GPopup.ButtonPos.Secondary, "離開");
                popup.GetComponent<GPopup>().OnButtonSecondaryPressed += GoToScene_Home;
            }

        } else
        {
            title = "返回選擇地圖";
            message = "確定要回到前一步驟嗎？返回後將會遺失你現在的採集進度！";
            popup.GetComponent<GPopup>().OnButtonPrimaryPressed += GoToScene_SelectMap;
        }

        popup.GetComponent<GPopup>().OpenPopup(title, message);
    }

    public void ForButtonNext()
    {
        string title = "收集完成";
        string message = "恭喜你已完成調茶原料的收集！";

        GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.General);
        popup.transform.parent = gameObject.transform;
        popup.GetComponent<GPopup>().SetButtonText(GPopup.ButtonPos.Primary, "下一步");
        popup.GetComponent<GPopup>().SetButtonText(GPopup.ButtonPos.Secondary, "我再逛逛");
        popup.GetComponent<GPopup>().OpenPopup(title, message);
        popup.GetComponent<GPopup>().OnButtonPrimaryPressed += GoToScene_NameTeaCard;

        //GameObject gPopup = Instantiate(Resources.Load("Prefab/Gpopup", typeof(GameObject)) as GameObject);
        //gPopup.transform.parent = gameObject.transform;
        //gPopup.GetComponent<GPopup>().OpenPopup(title, message);     
    }

    public void ForCartButton()
    {
        //用途：按下購物車按鈕時生成購物車彈窗

        GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.Cart);
        popup.transform.parent = gameObject.transform;
        popup.GetComponent<CartPopup>().OpenPopup();
        popup.GetComponent<CartPopup>().InstantiateCartItem(playerInventory.inventory);
        
    }

    public void ForProgressButton()
    {
        //用途：按下進度按鈕時生成進度彈窗

        GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.Progress);
        popup.transform.parent = gameObject.transform;
        popup.GetComponent<GProgressPopup>().OpenPopup();
    }

    public void ForTutorialButton()
    {
        //用途：按下教學按鈕時生成教學彈窗

        GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.Tutorial);
        popup.transform.parent = gameObject.transform;
        popup.GetComponent<TutorialPopup>().OpenPopup();
    }
}