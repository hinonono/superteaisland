using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIManager : MonoBehaviour
{
    //SCRIPT用途：開發中。最核心管理GUI的文件

    //Singleton 設定START
    private static GUIManager _instance;
    public static GUIManager Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject gameObj = new GameObject("GUI Manager Singleton");
                gameObj.AddComponent<GUIManager>();
            }

            return _instance;
        }
    }

    void Awake()
    {
        _instance = this;
        DontDestroyOnLoad(this);
    }
    //Singleton 設定END

    //屬性宣告
    public enum PopupType
    {
        General,
        Info,
        Pick,
        Cart,
        Progress,
        Tutorial,
        Destination,
        Service,
        Tableware,
        Staff
    }

    public Color color_primary = new Color(132, 79, 40, 255);
    public Color color_secondary = new Color(229, 176, 107, 255);

    //CORE FUNCTION
    public GameObject InstantiatePopup(PopupType type)
    {
        //FUNC用途：產生一個GPopup

        switch (type)
        {
            case PopupType.General:
                GameObject gPopup = Instantiate(Resources.Load("Prefab/GPopup", typeof(GameObject)) as GameObject);
                return gPopup;

            case PopupType.Info:
                GameObject infoPopup = Instantiate(Resources.Load("Prefab/InfoPopup", typeof(GameObject)) as GameObject);
                return infoPopup;

            case PopupType.Pick:
                GameObject itemPopup = Instantiate(Resources.Load("Prefab/ItemPopup", typeof(GameObject)) as GameObject);
                return itemPopup;

            case PopupType.Cart:
                GameObject cartPopup = Instantiate(Resources.Load("Prefab/GCartPopup", typeof(GameObject)) as GameObject);
                return cartPopup;

            case PopupType.Progress:
                GameObject progressPopup = Instantiate(Resources.Load("Prefab/GProgressPopup", typeof(GameObject)) as GameObject);
                return progressPopup;

            case PopupType.Tutorial:
                GameObject tutorialPopup = Instantiate(Resources.Load("Prefab/TutorialPopup", typeof(GameObject)) as GameObject);
                return tutorialPopup;

            case PopupType.Destination:
                GameObject desPopup = Instantiate(Resources.Load("Prefab/DestinationPopup", typeof(GameObject)) as GameObject);
                return desPopup;
            case PopupType.Service:
                GameObject servicePopup = Instantiate(Resources.Load("Prefab/ServicePopup", typeof(GameObject)) as GameObject);
                return servicePopup;

            case PopupType.Tableware:
                GameObject tablewarePopup = Instantiate(Resources.Load("Prefab/TablewarePopup", typeof(GameObject)) as GameObject);
                return tablewarePopup;

            case PopupType.Staff:
                GameObject staffPopup = Instantiate(Resources.Load("Prefab/StaffPopup", typeof(GameObject)) as GameObject);
                return staffPopup;


            default:
                Debug.Log("沒有找到你所指定的Popup種類，因此無法打開。");
                return null;
        }
    }

    public GameObject InstantiateDialogueItem()
    {
        GameObject dialogueItem = Instantiate(Resources.Load("Prefab/DialogueItem", typeof(GameObject)) as GameObject);
        return dialogueItem;
    }

    public GameObject InstantiateAlert()
    {
        //FUNC用途：產生一個GAlert

        GameObject alert = Instantiate(Resources.Load("Prefab/GAlert", typeof(GameObject)) as GameObject);
        return alert;
    }
}
