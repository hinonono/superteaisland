using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;
using Image = UnityEngine.UI.Image;

public class GUIManager_ServiceBell : MonoBehaviour
{
    public void ServiceBell()
    {
        //設定popup的標題與內文
        string title = "請問您需要甚麼協助呢?";
        string message = "告訴我們您的需求，以便讓店員更快速地服務";
        string subtitle = "沒看到需要的選項?";

        //生成一個popup
        GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.Service);

        //將生成的popup的unity hierachy設定於UI Canvas底下
        popup.transform.parent = gameObject.transform;

        //打開popup
        popup.GetComponent<ServicePopup>().ServiceBell(title, message, subtitle);


        //使popup特定按鈕按下的event附著於本script的其他function上
        popup.GetComponent<ServicePopup>().OnButtonTablewarePressed += Tableware;
        popup.GetComponent<ServicePopup>().OnButtonWaterPressed += Water;
        popup.GetComponent<ServicePopup>().OnButtonServicePressed += Notice;
        popup.GetComponent<ServicePopup>().OnButtonCallPressed += Staff;
    }
    public void Tableware()
    {
        //設定popup的標題與內文
        string title = "額外餐具";
        string content = "請填入需要的項目與數量後按下送出";
        string Name = "吸管";
        string Name2 = "杯子";
        string Name3 = "湯匙";


        //生成一個popup
        GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.Tableware);

        //將生成的popup的unity hierachy設定於UI Canvas底下
        popup.transform.parent = gameObject.transform;

        // GameObject Obj = GUIManager.Instance.InstantiateServiceItem();

        popup.GetComponent<TablewarePopup>().SetButtonText(TablewarePopup.ButtonPos.Primary, "送出");

        //打開popup
        popup.GetComponent<TablewarePopup>().OpenPopup(title, content, Name, Name2, Name3);

        // Obj.transform.parent = gameObject.transform;
        // popup.GetComponent<TablewarePopup>().ServiceBellAdd(Name, Name2, Name3);
        
    }

    // Update is called once per frame
    public void Water()
    {
        //設定popup的標題與內文
        string title = "水";
        string content = "請填入需要的項目與數量後按下送出";
        string Name = "冰水";
        string Name2 = "溫水";
        string Name3 = "常溫水";

        //生成一個popup
        GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.Tableware);

        //將生成的popup的unity hierachy設定於UI Canvas底下
        popup.transform.parent = gameObject.transform;

        // GameObject Obj = GUIManager.Instance.InstantiateServiceItem();

        popup.GetComponent<TablewarePopup>().SetButtonText(TablewarePopup.ButtonPos.Primary, "送出");

        //打開popup
        popup.GetComponent<TablewarePopup>().OpenPopup(title, content, Name, Name2, Name3);


    }
    public void Notice()
    {
        //設定popup的標題與內文
        string title = "已通知店員";
        string content = "請耐心等候店員為您服務";

        //生成一個popup
        GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.Staff);

        //將生成的popup的unity hierachy設定於UI Canvas底下
        popup.transform.parent = gameObject.transform;

        // GameObject Obj = GUIManager.Instance.InstantiateServiceItem();

        //打開popup
        popup.GetComponent<StaffPopup>().OpenPopup(title, content);


    }
    public void Staff()
    {
       string title = "已呼叫店員";
        string content = "請耐心等候店員為您服務";

        //生成一個popup
        GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.General);

        popup.GetComponent<GPopup>().SetButtonText(GPopup.ButtonPos.Primary, "取消");
        popup.GetComponent<GPopup>().SetButtonText(GPopup.ButtonPos.Secondary, "再次呼叫店員");

        //將生成的popup的unity hierachy設定於UI Canvas底下
        popup.transform.parent = gameObject.transform;


        //打開popup
        popup.GetComponent<GPopup>().OpenPopup(title, content);

    }
}
