using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
public class AboutTaiocha_GUIManager : MonoBehaviour
{
   //SCRIPT用途：在地圖場景控制UI按鈕可以去哪裡

   public float animateSpeed = 0.5f;
    public GameObject popup;

    private string sceneDestination;

    // public Transform box;
    // public CanvasGroup background;

    //public Text popup_areaName;
    //public Text popup_teaName;

    //嘗試簡化宣告
    public GameObject popup_content;
    // public GameObject topping_content;

    public void SetSceneDestination(string destination)
    {
        popup.SetActive(true);
        sceneDestination = destination;

        showPopup();

    }


    public void goToScene(string destination)
    {
        if (destination != null)
        {
            SceneManager.LoadScene(destination);
        }
        else
        {
            Debug.Log("Scene Destination is not set yet. Please fill in scene name first.");
        }
    }

        private void showPopup()
    {
        //VOID用途：將Popup打開

        // background.alpha = 0;
        // background.LeanAlpha(0, animateSpeed);

        // box.localPosition = new Vector2(0, 0);
        // box.LeanMoveLocalY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;
    }

        public void ClosePopup()
    {
        //VOID用途：將Popup關起來

        // background.LeanAlpha(0, animateSpeed);
        // box.LeanMoveLocalY(-Screen.height, animateSpeed).setEaseInExpo().setOnComplete(onComplete);
    }

    void onComplete()
    {
        popup.SetActive(false);
    }
}

