using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ToTeach_UIManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("GoToScene_Teach", 8.0f);
    }
    private void GoToScene_Teach()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.TeachTaio.ToString());
        Debug.Log("此次執行的時間為:" + Time.time);
    }
}
