using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GUIManager_Teach : MonoBehaviour
{

    public void ToStep1()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.TeachTaio.ToString());
    }
    public void ToStep2()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.TeachTaio1.ToString());
    }
    public void ToStep3()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.TeachTaio2.ToString());
    }
    public void ToStep4()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.TeachTaio3.ToString());
    }public void ToStep5()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.TeachTaio4.ToString());
    }
    public void ToStep6()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.TeachTaio5.ToString());
    }
    public void BackToHome()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.Home.ToString());
    }
}
