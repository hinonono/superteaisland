using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GUIManager_NameTeaCard : MonoBehaviour
{
    //SCRIPT用途：在命名茶卡場景中控制UI元素


    //DELEGATE


    //EVENT
    public event Action OnValueChanged;

    //屬性宣告
    [SerializeField] private GameObject confirmButton;
    [SerializeField] private GameObject card;
    [SerializeField] private TextMeshProUGUI customName;
    [SerializeField] private RecipeChecker recipeChecker;
    [SerializeField] private Text inputField_value;
    [SerializeField] private TextMeshProUGUI text_date;

    public float animateSpeed = 0.5f;
    private bool isCheckCompleted;
    private bool isInputCompleted;


    //OnEnable, OnDisable
    private void OnEnable()
    {
        recipeChecker.OnCheckCompleted += RecipeChecker_OnCheckCompleted;
    }

    private void OnDisable()
    {
        recipeChecker.OnCheckCompleted -= RecipeChecker_OnCheckCompleted;
    }

    //Awake, Start, Update
    private void Start()
    {
        confirmButton.GetComponent<Button>().interactable = false;

        DateTime dateTime = DateTime.Now;
        text_date.text = dateTime.Year.ToString() + "." + dateTime.Month.ToString() + "." + dateTime.Day.ToString();
    }


    private void Update()
    {
        customName.text = inputField_value.text;


        if (isCheckCompleted && isInputCompleted)
        {
            if (confirmButton.GetComponent<Button>().interactable != true)
            {
                confirmButton.GetComponent<Button>().interactable = true;
            }
        }
    }

    //CORE FUNCTION
    private void showCard(string name)
    {
        string path = "GUI/TeaCard/teacard_" + name;
        Debug.Log(path);
        card.GetComponentInChildren<Image>().sprite = Resources.Load(path, typeof(Sprite)) as Sprite;

        LeanTween.alphaCanvas(card.GetComponent<CanvasGroup>(), 1.0f, 2.0f);
    }

    private void RecipeChecker_OnCheckCompleted(string name)
    {
        isCheckCompleted = true;
        showCard(name);
    }


    //UNITY EDITOR FUNCTION
    public void onValueChanged()
    {
        inputField_value.text = customName.text;
        OnValueChanged?.Invoke();
    }

    public void onInputCompleted()
    {
        isInputCompleted = true;
    }

    public void ForConfirmButton()
    {
        gameObject.GetComponent<RecipeChecker>().AddTiaocha(inputField_value.text);
        SceneManager.LoadScene(SceneSingleton.SceneList.Temple.ToString());
    }
}