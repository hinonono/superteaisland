using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;

public class GUIManager_Nojoystick : MonoBehaviour
{
    //SCRIPT用途：開發中。附加於GUI CANVAS之上，管理所有的UI元件。


    //DELEGATE


    //EVENT


    //屬性宣告
    private enum PopupTypeList
    {
        General,
        Info,
        Pick
    }

    private ItemObject currentItem;
    private GameObject[] walls;
    private GameObject[] hintAreas;
    [SerializeField] private PlayerInventory playerInventory;


    //OnEnable, OnDisable
    private void OnEnable()
    {
        
    }

    private void OnDisable()
    {
        
    }


    //Awake, Start, Update
    private void Awake()
    {

    }

    private void Start()
    {
        ShowAlertWhenSceneLoaded();
    }


    //CORE FUNCTION
    

    private void ShowAlertWhenSceneLoaded()
    {
        if (SceneSingleton.Instance.getCurrentSceneName() == SceneSingleton.SceneList.Temple.ToString())
        {
            GameObject alert = GUIManager.Instance.InstantiateAlert();
            alert.transform.parent = gameObject.transform;
            alert.GetComponent<GAlert>().ShowAlert("請前往祭壇上方去拜見長老", 5);
        }
        else
        {
            GameObject alert = GUIManager.Instance.InstantiateAlert();
            alert.transform.parent = gameObject.transform;
            alert.GetComponent<GAlert>().ShowAlert("請選擇一個想要探索的場景", 5);
        }
    }

    //private void SubscribeToHintArea()
    //{
    //    //VOID用途：訂閱Hint Area

    //    //if (findHintArea)
    //    //{
    //    //    Debug.Log("Tea Alert will search for HintArea in the scene now.");
    //    //    hintAreas = GameObject.FindGameObjectsWithTag("HintArea");
    //    //    if (hintAreas == null)
    //    //    {
    //    //        Debug.Log("hintAreas array is empty");
    //    //    }
    //    //    else
    //    //    {
    //    //        for (int i = 0; i < hintAreas.Length; i++)
    //    //        {
    //    //            hintAreas[i].GetComponent<HintArea>().onHintAreaTouched += HintArea_onHintAreaTouched;
    //    //        }
    //    //    }
    //    //}
    //}

    //private void SubscribeToWall()
    //{
    //    //VOID用途：訂閱牆壁的事件

    //    walls = GameObject.FindGameObjectsWithTag("Wall");
    //    for (int i = 0; i < walls.Length; i++)
    //    {
    //        walls[i].GetComponent<WallDetecter>().onWallTouched += WallDetecter_onWallTouched;
    //    }
    //}

    //private void WallDetecter_onWallTouched(object sender, EventArgs e)
    //{
    //    string[] texts =
    //    {
    //        "似乎有股神秘的力量阻擋我們不能夠再往前進...",
    //        "前方似乎沒有路了，換個方向前進試試看吧！"
    //    };

    //    string text = texts[Random.Range(0, texts.Length)];
    //    InstantiateAlert(text);
    //}

    //private void HintArea_onHintAreaTouched(string text)
    //{
    //    InstantiateAlert(text);
    //}

    //private void SelectTarget_onItemTouched(GroundItem item)
    //{
    //    currentItem = item.item;
    //    InstantiatePopup(PopupTypeList.Info);
    //}

    //private void SelectTarget_onItemHold(GroundItem item)
    //{
    //    currentItem = item.item;
    //    InstantiatePopup(PopupTypeList.Pick);
    //}

    //private void SelectTarget_onItemIntendToInteract()
    //{
    //    string message = "你需要靠近一點才能進行互動";
    //    InstantiateAlert(message);
    //}

    private void SelectTarget_onPersonTouched()
    {
        string title = "採集完畢?";
        string content = "若須2杯以上的茶品，請選擇繼續採茶進行點餐";

        GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.General);
        popup.transform.parent = gameObject.transform;
        popup.GetComponent<GPopup>().OpenPopup(title, content);
        popup.GetComponent<GPopup>().OnButtonSecondaryPressed += CancelOrder;

        //Sprite primary = Resources.Load("GUI/Button/gocheckout", typeof(Sprite)) as Sprite;
        //Sprite secondary = Resources.Load("GUI/Button/continue", typeof(Sprite)) as Sprite;

        //GameObject gPopup = Instantiate(Resources.Load("Prefab/GPopup", typeof(GameObject)) as GameObject);
        //gPopup.transform.parent = gameObject.transform;

        //gPopup.GetComponent<GPopup>().SetButton(primary, GPopup.ButtonPos.Primary);
        //gPopup.GetComponent<GPopup>().SetButton(secondary, GPopup.ButtonPos.Secondary);
        //gPopup.GetComponent<GPopup>().OpenPopup(title, content);
    }

    private void CancelOrder()
    {
        GoToScene_Home();
    }

    private void PlayerInventory_onCollectCompleted()
    {
        //nextButton.interactable = true;
        ForButtonNext();
    }

    private void GoToScene_Home()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.Home.ToString());
    }

    private void GoToScene_NameTeaCard()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.NameTeaCard.ToString());
    }


    //UNITY EDITOR FUNCTION
    public void GoToScence(string des)
    {
        SceneManager.LoadScene(des);
    }

    public void ForButtonBack()
    {

        if (playerInventory.inventory.Container.Items.Count != 0)
        {
            string title = "返回主選單";
            string message = "您的購物車裡有尚未結帳的商品。如果回到前一步，購物車將會被重置！";

            GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.General);
            popup.transform.parent = gameObject.transform;
            popup.GetComponent<GPopup>().OpenPopup(title, message);
            popup.GetComponent<GPopup>().OnButtonPrimaryPressed += GoToScene_Home;
        } else
        {
            string title = "取消遊戲";
            string message = "確認要結束遊戲模式？";

            GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.General);
            popup.transform.parent = gameObject.transform;
            popup.GetComponent<GPopup>().OpenPopup(title, message);

            popup.GetComponent<GPopup>().SetButtonText(GPopup.ButtonPos.Primary, "我再想想");
            popup.GetComponent<GPopup>().SetButtonText(GPopup.ButtonPos.Secondary, "離開");
            popup.GetComponent<GPopup>().OnButtonSecondaryPressed += GoToScene_Home;
        }
    }

    public void ForButtonNext()
    {
        string title = "已完成收集";
        string message = "恭喜你已完成調茶原料的收集！在進入祭壇前，先替你的這杯調茶命名吧！待會和長老介紹時要用到呢。";

        GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.General);
        popup.transform.parent = gameObject.transform;
        popup.GetComponent<GPopup>().SetButtonText(GPopup.ButtonPos.Primary, "前往命名");
        popup.GetComponent<GPopup>().SetButtonText(GPopup.ButtonPos.Secondary, "稍後");
        popup.GetComponent<GPopup>().OpenPopup(title, message);
        popup.GetComponent<GPopup>().OnButtonPrimaryPressed += GoToScene_NameTeaCard;
    }

    public void ForCartButton()
    {
        //用途：按下購物車按鈕時生成購物車彈窗

        GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.Cart);
        popup.transform.parent = gameObject.transform;
        popup.GetComponent<CartPopup>().OpenPopup();
        popup.GetComponent<CartPopup>().InstantiateCartItem(playerInventory.inventory);

    }
}