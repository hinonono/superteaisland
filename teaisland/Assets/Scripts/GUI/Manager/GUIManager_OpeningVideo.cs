using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GUIManager_OpeningVideo : MonoBehaviour
{
    //SCRIPT用途：管理前導動畫的元件動作


    //DELEGATE


    //EVENT


    //屬性宣告


    //OnEnable, OnDisable


    //Awake, Start, Update


    //CORE FUNCTION


    //UNITY EDITOR FUNCTION
    public void ForButtonBack()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.Home.ToString());
    }
}
