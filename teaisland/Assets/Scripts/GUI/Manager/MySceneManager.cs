using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MySceneManager : MonoBehaviour
{
    //SCRIPT用途：


    //DELEGATE


    //EVENT


    //屬性宣告
    //public enum LoadGUIType
    //{
    //    CANVAS_GAMEORDER,
    //    CANVAS_NOJOYSTICK
    //}

    //public LoadGUIType type;

    public bool showGameOperationUI = true;
    public bool showInventoryCanvas = true;


    //OnEnable, OnDisable


    //Awake, Start, Update
    private void Start()
    {
        StartCoroutine(LoadMyScene());   
    }


    //CORE FUNCTION
    IEnumerator LoadMyScene()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync("CANVAS_GAMEORDER", LoadSceneMode.Additive);
        yield return async;

        GameObject ui = GameObject.Find("UI");
        ui.GetComponent<GUIManager_Gameorder>().showGameOperationUI = showGameOperationUI;
        ui.GetComponent<GUIManager_Gameorder>().showInventoryCanvas = showInventoryCanvas;
    }


    //UNITY EDITOR FUNCTION
}
