using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GUIManager_SelectMap : MonoBehaviour
{
    //SCRIPT用途：


    //DELEGATE


    //EVENT


    //屬性宣告
    public bool showGameOperationUI = true;
    public bool showInventoryCanvas = true;

    private GameObject ui;


    //OnEnable, OnDisable


    //Awake, Start, Update
    private void Start()
    {
        StartCoroutine(SceneInit());
    }


    //CORE FUNCTION
    IEnumerator SceneInit()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync("CANVAS_GAMEORDER", LoadSceneMode.Additive);
        yield return async;

        ui = GameObject.Find("UI");
        ui.GetComponent<GUIManager_Gameorder>().showGameOperationUI = showGameOperationUI;
        ui.GetComponent<GUIManager_Gameorder>().showInventoryCanvas = showInventoryCanvas;

        DialogueTrigger trigger = gameObject.GetComponent<DialogueTrigger>();
        trigger.StartDialogue();
    }

    private void GoToScene(int id)
    {
        switch (id)
        {
            case 0:
                SceneManager.LoadScene("Map_red");
                break;
            case 1:
                SceneManager.LoadScene("Map_yellow");
                break;
            case 2:
                SceneManager.LoadScene("Map_cyan");
                break;
            case 3:
                SceneManager.LoadScene("Map_black");
                break;
            case 4:
                SceneManager.LoadScene("Map_white");
                break;
            case 5:
                SceneManager.LoadScene("Map_green");
                break;
            default:
                break;
        }
    }

    //UNITY EDITOR FUNCTION
    public void ForMapButton(int id)
    {
        GameObject popup = GUIManager.Instance.InstantiatePopup(GUIManager.PopupType.Destination);
        popup.transform.parent = ui.transform;

        popup.GetComponent<DestinationPopup>().OpenPopup(id);
        popup.GetComponent<DestinationPopup>().OnButtonPrimaryPressed += GoToScene;
    }
}
