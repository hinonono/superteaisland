using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class SelectMapButtonManager : MonoBehaviour
{

    //SCRIPT用途：在地圖場景控制UI按鈕可以去哪裡

    public float animateSpeed = 0.5f;
    public GameObject popup;
    private string sceneDestination;

    public Transform box;
    public CanvasGroup background;

    //public Text popup_areaName;
    //public Text popup_teaName;
    public Image popup_sceneImage;

    //嘗試簡化宣告
    public GameObject popup_content;
    public GameObject topping_content;
    private string pure_str;

    //那個JSON的Script
    private MenuReader menuReader;

    public Sprite[] sceneSnapshot;

    private void Start()
    {
        menuReader = gameObject.GetComponent<MenuReader>();
    }

    public void SetSceneDestination(string destination)
    {
        popup.SetActive(true);
        SceneSingleton.Instance.destination = destination;
        

        switch (destination)
        {
            case "Map_red":
                setPopupText(0);
                break;

            case "Map_yellow":
                setPopupText(1);
                break;

            case "Map_cyan":
                setPopupText(2);
                break;

            case "Map_black":
                setPopupText(3);
                break;

            case "Map_white":
                setPopupText(4);
                break;

            case "Map_green":
                setPopupText(5);
                break;

            default:
                setPopupText(0);
                break;
        }

        showPopup();

    }

    private void setPopupText(int id)
    {
        //VOID用途：依據不同的場景設定POPUP中的文字

        //把該宣告的子項目宣告出來
        Text able_list = popup_content.transform.Find("Text_AbleList").GetComponent<Text>();
        Text tea_flavor = popup_content.transform.Find("Text_Tea_Flavor").GetComponent<Text>();
        Text tea_effect = popup_content.transform.Find("Text_Tea_Effect").GetComponent<Text>();

        TextMeshProUGUI area_name = popup_content.transform.Find("Text_Area_Name").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI text_tea = popup_content.transform.Find("Text_Tea").GetComponent<TextMeshProUGUI>();


        TextMeshProUGUI topping_1 = topping_content.transform.GetChild(0).GetChild(1).gameObject.GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI topping_2 = topping_content.transform.GetChild(1).GetChild(1).gameObject.GetComponent<TextMeshProUGUI>();

        

        for (var i = 0; i < menuReader.myuiteaList.uitea[id].uinfo.coordination.Length; i++)
        {
            pure_str += menuReader.myuiteaList.uitea[id].uinfo.coordination[i].ToString();
            if (i < menuReader.myuiteaList.uitea[id].uinfo.coordination.Length - 1 )
            {
                pure_str += ", ";
            }
            
        }

        Image topping_1_image = topping_content.transform.GetChild(0).GetChild(0).GetChild(0).gameObject.GetComponent<Image>();
        Image topping_2_image = topping_content.transform.GetChild(1).GetChild(0).GetChild(0).gameObject.GetComponent<Image>();

        string topping_1_img_path = "Item/topping/item_topping_" + menuReader.myuiteaList.uitea[id].uitopping[0].imagepath;
        string topping_2_img_path = "Item/topping/item_topping_" + menuReader.myuiteaList.uitea[id].uitopping[1].imagepath;

        var sprite_1 = Resources.Load<Sprite>(topping_1_img_path);
        var sprite_2 = Resources.Load<Sprite>(topping_2_img_path);

        topping_1_image.sprite = sprite_1;
        topping_2_image.sprite = sprite_2;


        Image tea_image = popup_content.transform.Find("Tea_1").GetComponentInChildren<Image>();
        string tea_img_path = "Item/tea/item_tea_" + menuReader.myuiteaList.uitea[id].imagepath;
        var sprite_3 = Resources.Load<Sprite>(tea_img_path);
        tea_image.sprite = sprite_3;


        area_name.text = menuReader.myuiteaList.uitea[id].areaname;
        able_list.text = pure_str;
        tea_flavor.text = menuReader.myuiteaList.uitea[id].uinfo.flavor;
        tea_effect.text = menuReader.myuiteaList.uitea[id].uinfo.effect;
        topping_1.text = menuReader.myuiteaList.uitea[id].uitopping[0].name;
        topping_2.text = menuReader.myuiteaList.uitea[id].uitopping[1].name;
        text_tea.text = menuReader.myuiteaList.uitea[id].name;
        popup_sceneImage.sprite = sceneSnapshot[id];
    }


    public void ButtonMoveScene()
    {
        //if (sceneDestination != null)
        //{
        //    SceneManager.LoadScene(sceneDestination);
        //}
        //else
        //{
        //    Debug.Log("Scene Destination is not set yet. Try run SetSceneDestination() first.");
        //}

        if (SceneSingleton.Instance.getCurrentDestination() != null)
        {
            SceneSingleton.Instance.LoadSceneByDestination();
        } else
        {
            Debug.LogError("無法自Scene Singleton中設定的目的地載入場景。");
        }
        
    }

    public void goToScene(string destination)
    {
        if (destination != null)
        {
            SceneManager.LoadScene(destination);
        }
        else
        {
            Debug.Log("Scene Destination is not set yet. Please fill in scene name first.");
        }
    }

    private void showPopup()
    {
        //VOID用途：將Popup打開

        background.alpha = 0;
        background.LeanAlpha(1, animateSpeed);

        box.localPosition = new Vector2(0, -Screen.height);
        box.LeanMoveLocalY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;
    }

    public void ClosePopup()
    {
        //VOID用途：將Popup關起來

        background.LeanAlpha(0, animateSpeed);
        box.LeanMoveLocalY(-Screen.height, animateSpeed).setEaseInExpo().setOnComplete(onComplete);
    }

    void onComplete()
    {
        pure_str = "";
        popup.SetActive(false);
    }
}
