using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Home_GUI : MonoBehaviour
{
    [SerializeField] private string _tableNum;
    [SerializeField] private TextMeshProUGUI tableNum;
    [SerializeField] private TextMeshProUGUI time;
    //[SerializeField] private TextMeshProUGUI checkout;
    [SerializeField] private int price;

    private int currentMin;
    

    private void Awake()
    {
        tableNum.text = _tableNum;
        UpdateTimeString();

        currentMin = DateTime.Now.Minute;

        //checkout.text = "結帳($" + price + ")";
    }

    private void Update()
    {
        if (DateTime.Now.Minute != currentMin)
        {
           UpdateTimeString();
        }
    }

    private void UpdateTimeString()
    {
        DateTime dateTime = DateTime.Now;
        time.text = dateTime.Hour.ToString() + ":" + dateTime.Minute.ToString();
    }
}
