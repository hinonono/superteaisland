using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class QuickInfoPopupManager : MonoBehaviour
{

    //SCRIPT用途：在地圖場景控制UI按鈕可以去哪裡

    public float animateSpeed = 0.5f;
    public GameObject popup;
    private string sceneDestination;

    public Transform box;
    public CanvasGroup background;

    //public Text popup_areaName;
    //public Text popup_teaName;

    // //嘗試簡化宣告
    // public GameObject popup_content;
    // // public GameObject topping_content;
    // private string pure_str;

    // //那個JSON的Script
    // private MenuReader menuReader;


    private void Start()
    {
        // menuReader = gameObject.GetComponent<MenuReader>();
    }

    public void SetSceneDestination(string destination)
    {
        popup.SetActive(true);
        sceneDestination = destination;
        

        // switch (destination)
        // {
        //     case "Quick_red":
        //         setPopupText(0);
        //         break;

        //     case "Quick_yellow":
        //         setPopupText(1);
        //         break;

        //     case "Quick_cyan":
        //         setPopupText(2);
        //         break;

        //     case "Quick_black":
        //         setPopupText(3);
        //         break;

        //     case "Quick_white":
        //         setPopupText(4);
        //         break;

        //     case "Quick_green":
        //         setPopupText(5);
        //         break;

        //     default:
        //         setPopupText(0);
        //         break;
        // }

        showPopup();

    }

    // private void setPopupText(int id)
    // {
    //     //VOID用途：依據不同的場景設定POPUP中的文字

    //     //把該宣告的子項目宣告出來
    //     TextMeshProUGUI able_list = popup_content.transform.Find("Text_AbleList").GetComponent<TextMeshProUGUI>();
    //     TextMeshProUGUI tea_flavor = popup_content.transform.Find("Text_Tea_Flavor").GetComponent<TextMeshProUGUI>();
    //     TextMeshProUGUI tea_effect = popup_content.transform.Find("Text_Tea_Effect").GetComponent<TextMeshProUGUI>();

    //     TextMeshProUGUI text_tea = popup_content.transform.Find("Text_Tea").GetComponent<TextMeshProUGUI>();
      

    //     for (var i = 0; i < menuReader.myuiteaList.uitea[id].uinfo.coordination.Length; i++)
    //     {
    //         pure_str += menuReader.myuiteaList.uitea[id].uinfo.coordination[i].ToString();
    //         if (i < menuReader.myuiteaList.uitea[id].uinfo.coordination.Length - 1 )
    //         {
    //             pure_str += ", ";
    //         }
            
    //     }

    //     text_tea.text = menuReader.myuiteaList.uitea[id].name; 
    //     tea_flavor.text = menuReader.myuiteaList.uitea[id].uinfo.flavor;
    //     able_list.text = pure_str;
    //   //   able_list.text = menuReader.myuiteaList.uitea[id].;
    //     tea_effect.text = menuReader.myuiteaList.uitea[id].uinfo.effect;

    //     Debug.Log(menuReader.myuiteaList.uitea[id].areaname);

    // }

public void ButtonMoveScene()
    {
        if (sceneDestination != null)
        {
            SceneManager.LoadScene(sceneDestination);
        }
        else
        {
            Debug.Log("Scene Destination is not set yet. Try run SetSceneDestination() first.");
        }
        
    }

    public void goToScene(string destination)
    {
        if (destination != null)
        {
            SceneManager.LoadScene(destination);
        }
        else
        {
            Debug.Log("Scene Destination is not set yet. Please fill in scene name first.");
        }
    }

    private void showPopup()
    {
        //VOID用途：將Popup打開

        background.alpha = 0;
        background.LeanAlpha(1, animateSpeed);

        box.localPosition = new Vector2(0, -Screen.height);
        box.LeanMoveLocalY(0, animateSpeed).setEaseOutExpo().delay = 0.1f;
    }

    public void ClosePopup()
    {
        //VOID用途：將Popup關起來

        background.LeanAlpha(0, animateSpeed);
        box.LeanMoveLocalY(-Screen.height, animateSpeed).setEaseInExpo().setOnComplete(onComplete);
    }

    void onComplete()
    {
        // pure_str = "";
        popup.SetActive(false);
    }
}

