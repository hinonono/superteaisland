using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pay_UIManager : MonoBehaviour
{
    public void LinePay()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.Quick_linePay.ToString());
    }
    public void Cash()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.Quick_cash.ToString());
    }
    public void BackToTea()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.Quick_tea.ToString());
    }
     public void BackToPay()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.Quick_pay.ToString());
    }
    public void BackToHome()
    {
        SceneManager.LoadScene(SceneSingleton.SceneList.Home.ToString());
    }
}